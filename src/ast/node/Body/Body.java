package ast.Node.Body;

import ast.Node.ParentNode;
import ast.Node.Scope;

public class Body extends ParentNode {
    String nameBody;
    BodyBody bodyBody;

    String center;


    public Body() {
    }

    public Body(String nameBody, BodyBody bodyBody) {
        this.nameBody = nameBody;
        this.bodyBody = bodyBody;
    }

    public Body(String nameBody, String center, BodyBody bodyBody, Scope scope) {
        this.nameBody = nameBody;
        this.bodyBody = bodyBody;
        this.center = center;
        super.setScope(scope);
    }

    public String getNameBody() {
        return nameBody;
    }

    public void setNameBody(String nameBody) {
        this.nameBody = nameBody;
    }


    public BodyBody getBodyBody() {
        return bodyBody;
    }

    public void setBodyBody(BodyBody bodyBody) {
        this.bodyBody = bodyBody;
    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }

    public String getCenter() {
        return center;
    }

    public void setCenter(String center) {
        this.center = center;
    }

}
