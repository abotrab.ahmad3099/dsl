package ast.Node.Body.Form.BodyForm;

import ast.Node.ParentNode;
import ast.Node.Scope;

public class OnPressed extends ParentNode {


    String event;
    String nameFileControllerOrFun;

    public OnPressed() {
    }

    public OnPressed(Scope scope, String event, String nameFileControllerOrFun) {
        super.setScope(scope);
        this.event = event;
        this.nameFileControllerOrFun = nameFileControllerOrFun;
    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }

    public String getNameFileControllerOrFun() {
        return nameFileControllerOrFun;
    }

    public void setNameFileControllerOrFun(String nameFileControllerOrFun) {
        this.nameFileControllerOrFun = nameFileControllerOrFun;
    }

    public String getEvent() {
        return event;
    }

    public void setEvent(String event) {
        this.event = event;
    }


}
