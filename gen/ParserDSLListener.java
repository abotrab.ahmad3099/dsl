// Generated from /Users/ahmadabotrab/Desktop/Compiler_2_Project/dsl/src/ParserDSL.g4 by ANTLR 4.10.1
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ParserDSL}.
 */
public interface ParserDSLListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link ParserDSL#page}.
	 * @param ctx the parse tree
	 */
	void enterPage(ParserDSL.PageContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParserDSL#page}.
	 * @param ctx the parse tree
	 */
	void exitPage(ParserDSL.PageContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParserDSL#title}.
	 * @param ctx the parse tree
	 */
	void enterTitle(ParserDSL.TitleContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParserDSL#title}.
	 * @param ctx the parse tree
	 */
	void exitTitle(ParserDSL.TitleContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParserDSL#attributeInPage}.
	 * @param ctx the parse tree
	 */
	void enterAttributeInPage(ParserDSL.AttributeInPageContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParserDSL#attributeInPage}.
	 * @param ctx the parse tree
	 */
	void exitAttributeInPage(ParserDSL.AttributeInPageContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParserDSL#navbar}.
	 * @param ctx the parse tree
	 */
	void enterNavbar(ParserDSL.NavbarContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParserDSL#navbar}.
	 * @param ctx the parse tree
	 */
	void exitNavbar(ParserDSL.NavbarContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParserDSL#body}.
	 * @param ctx the parse tree
	 */
	void enterBody(ParserDSL.BodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParserDSL#body}.
	 * @param ctx the parse tree
	 */
	void exitBody(ParserDSL.BodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParserDSL#footer}.
	 * @param ctx the parse tree
	 */
	void enterFooter(ParserDSL.FooterContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParserDSL#footer}.
	 * @param ctx the parse tree
	 */
	void exitFooter(ParserDSL.FooterContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParserDSL#bodyNavBar}.
	 * @param ctx the parse tree
	 */
	void enterBodyNavBar(ParserDSL.BodyNavBarContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParserDSL#bodyNavBar}.
	 * @param ctx the parse tree
	 */
	void exitBodyNavBar(ParserDSL.BodyNavBarContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParserDSL#bodyBody}.
	 * @param ctx the parse tree
	 */
	void enterBodyBody(ParserDSL.BodyBodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParserDSL#bodyBody}.
	 * @param ctx the parse tree
	 */
	void exitBodyBody(ParserDSL.BodyBodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParserDSL#bodyFooter}.
	 * @param ctx the parse tree
	 */
	void enterBodyFooter(ParserDSL.BodyFooterContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParserDSL#bodyFooter}.
	 * @param ctx the parse tree
	 */
	void exitBodyFooter(ParserDSL.BodyFooterContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParserDSL#htmlMisc}.
	 * @param ctx the parse tree
	 */
	void enterHtmlMisc(ParserDSL.HtmlMiscContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParserDSL#htmlMisc}.
	 * @param ctx the parse tree
	 */
	void exitHtmlMisc(ParserDSL.HtmlMiscContext ctx);
	/**
	 * Enter a parse tree produced by {@link ParserDSL#htmlComment}.
	 * @param ctx the parse tree
	 */
	void enterHtmlComment(ParserDSL.HtmlCommentContext ctx);
	/**
	 * Exit a parse tree produced by {@link ParserDSL#htmlComment}.
	 * @param ctx the parse tree
	 */
	void exitHtmlComment(ParserDSL.HtmlCommentContext ctx);
}