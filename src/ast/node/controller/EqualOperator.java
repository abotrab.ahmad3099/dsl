package ast.Node.controller;

import ast.Node.ParentNode;
import ast.Node.Scope;


public class EqualOperator extends ParentNode {
    String textOperator;

    public EqualOperator() {
    }

    public EqualOperator(String textOperator, Scope scope) {
        this.textOperator = textOperator;
        super.setScope(scope);
    }

    public String getTextOperator() {
        return textOperator;
    }

    public void setTextOperator(String textOperator) {
        this.textOperator = textOperator;
    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }


}
