// Generated from /Users/ahmadabotrab/Desktop/Compiler_2_Project/dsl/src/ParserDSL.g4 by ANTLR 4.10.1
package generated;

import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link ParserDSL}.
 */
public interface ParserDSLListener extends ParseTreeListener {
    /**
     * Enter a parse tree produced by {@link ParserDSL#doc}.
     *
     * @param ctx the parse tree
     */
    void enterDoc(ParserDSL.DocContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#doc}.
     *
     * @param ctx the parse tree
     */
    void exitDoc(ParserDSL.DocContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#page}.
     *
     * @param ctx the parse tree
     */
    void enterPage(ParserDSL.PageContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#page}.
     *
     * @param ctx the parse tree
     */
    void exitPage(ParserDSL.PageContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#icon}.
     *
     * @param ctx the parse tree
     */
    void enterIcon(ParserDSL.IconContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#icon}.
     *
     * @param ctx the parse tree
     */
    void exitIcon(ParserDSL.IconContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#title}.
     *
     * @param ctx the parse tree
     */
    void enterTitle(ParserDSL.TitleContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#title}.
     *
     * @param ctx the parse tree
     */
    void exitTitle(ParserDSL.TitleContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#attributeInPage}.
     *
     * @param ctx the parse tree
     */
    void enterAttributeInPage(ParserDSL.AttributeInPageContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#attributeInPage}.
     *
     * @param ctx the parse tree
     */
    void exitAttributeInPage(ParserDSL.AttributeInPageContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#navbar}.
     *
     * @param ctx the parse tree
     */
    void enterNavbar(ParserDSL.NavbarContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#navbar}.
     *
     * @param ctx the parse tree
     */
    void exitNavbar(ParserDSL.NavbarContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#body}.
     *
     * @param ctx the parse tree
     */
    void enterBody(ParserDSL.BodyContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#body}.
     *
     * @param ctx the parse tree
     */
    void exitBody(ParserDSL.BodyContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#footer}.
     *
     * @param ctx the parse tree
     */
    void enterFooter(ParserDSL.FooterContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#footer}.
     *
     * @param ctx the parse tree
     */
    void exitFooter(ParserDSL.FooterContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#bodyNavBar}.
     *
     * @param ctx the parse tree
     */
    void enterBodyNavBar(ParserDSL.BodyNavBarContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#bodyNavBar}.
     *
     * @param ctx the parse tree
     */
    void exitBodyNavBar(ParserDSL.BodyNavBarContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#bodyBody}.
     *
     * @param ctx the parse tree
     */
    void enterBodyBody(ParserDSL.BodyBodyContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#bodyBody}.
     *
     * @param ctx the parse tree
     */
    void exitBodyBody(ParserDSL.BodyBodyContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#bodyFooter}.
     *
     * @param ctx the parse tree
     */
    void enterBodyFooter(ParserDSL.BodyFooterContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#bodyFooter}.
     *
     * @param ctx the parse tree
     */
    void exitBodyFooter(ParserDSL.BodyFooterContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#styleNavBar}.
     *
     * @param ctx the parse tree
     */
    void enterStyleNavBar(ParserDSL.StyleNavBarContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#styleNavBar}.
     *
     * @param ctx the parse tree
     */
    void exitStyleNavBar(ParserDSL.StyleNavBarContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#styleBody}.
     *
     * @param ctx the parse tree
     */
    void enterStyleBody(ParserDSL.StyleBodyContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#styleBody}.
     *
     * @param ctx the parse tree
     */
    void exitStyleBody(ParserDSL.StyleBodyContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#form}.
     *
     * @param ctx the parse tree
     */
    void enterForm(ParserDSL.FormContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#form}.
     *
     * @param ctx the parse tree
     */
    void exitForm(ParserDSL.FormContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#attributeForm}.
     *
     * @param ctx the parse tree
     */
    void enterAttributeForm(ParserDSL.AttributeFormContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#attributeForm}.
     *
     * @param ctx the parse tree
     */
    void exitAttributeForm(ParserDSL.AttributeFormContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#action}.
     *
     * @param ctx the parse tree
     */
    void enterAction(ParserDSL.ActionContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#action}.
     *
     * @param ctx the parse tree
     */
    void exitAction(ParserDSL.ActionContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#method}.
     *
     * @param ctx the parse tree
     */
    void enterMethod(ParserDSL.MethodContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#method}.
     *
     * @param ctx the parse tree
     */
    void exitMethod(ParserDSL.MethodContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#target}.
     *
     * @param ctx the parse tree
     */
    void enterTarget(ParserDSL.TargetContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#target}.
     *
     * @param ctx the parse tree
     */
    void exitTarget(ParserDSL.TargetContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#autoComplete}.
     *
     * @param ctx the parse tree
     */
    void enterAutoComplete(ParserDSL.AutoCompleteContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#autoComplete}.
     *
     * @param ctx the parse tree
     */
    void exitAutoComplete(ParserDSL.AutoCompleteContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#bodyForm}.
     *
     * @param ctx the parse tree
     */
    void enterBodyForm(ParserDSL.BodyFormContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#bodyForm}.
     *
     * @param ctx the parse tree
     */
    void exitBodyForm(ParserDSL.BodyFormContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#button}.
     *
     * @param ctx the parse tree
     */
    void enterButton(ParserDSL.ButtonContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#button}.
     *
     * @param ctx the parse tree
     */
    void exitButton(ParserDSL.ButtonContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#gotoRule}.
     *
     * @param ctx the parse tree
     */
    void enterGotoRule(ParserDSL.GotoRuleContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#gotoRule}.
     *
     * @param ctx the parse tree
     */
    void exitGotoRule(ParserDSL.GotoRuleContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#onPressedRule}.
     *
     * @param ctx the parse tree
     */
    void enterOnPressedRule(ParserDSL.OnPressedRuleContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#onPressedRule}.
     *
     * @param ctx the parse tree
     */
    void exitOnPressedRule(ParserDSL.OnPressedRuleContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#radio}.
     *
     * @param ctx the parse tree
     */
    void enterRadio(ParserDSL.RadioContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#radio}.
     *
     * @param ctx the parse tree
     */
    void exitRadio(ParserDSL.RadioContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#dateTime}.
     *
     * @param ctx the parse tree
     */
    void enterDateTime(ParserDSL.DateTimeContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#dateTime}.
     *
     * @param ctx the parse tree
     */
    void exitDateTime(ParserDSL.DateTimeContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#password}.
     *
     * @param ctx the parse tree
     */
    void enterPassword(ParserDSL.PasswordContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#password}.
     *
     * @param ctx the parse tree
     */
    void exitPassword(ParserDSL.PasswordContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#textFieldRule}.
     *
     * @param ctx the parse tree
     */
    void enterTextFieldRule(ParserDSL.TextFieldRuleContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#textFieldRule}.
     *
     * @param ctx the parse tree
     */
    void exitTextFieldRule(ParserDSL.TextFieldRuleContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#value}.
     *
     * @param ctx the parse tree
     */
    void enterValue(ParserDSL.ValueContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#value}.
     *
     * @param ctx the parse tree
     */
    void exitValue(ParserDSL.ValueContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#hintR}.
     *
     * @param ctx the parse tree
     */
    void enterHintR(ParserDSL.HintRContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#hintR}.
     *
     * @param ctx the parse tree
     */
    void exitHintR(ParserDSL.HintRContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#controllerRule}.
     *
     * @param ctx the parse tree
     */
    void enterControllerRule(ParserDSL.ControllerRuleContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#controllerRule}.
     *
     * @param ctx the parse tree
     */
    void exitControllerRule(ParserDSL.ControllerRuleContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#variableDefine}.
     *
     * @param ctx the parse tree
     */
    void enterVariableDefine(ParserDSL.VariableDefineContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#variableDefine}.
     *
     * @param ctx the parse tree
     */
    void exitVariableDefine(ParserDSL.VariableDefineContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#expression}.
     *
     * @param ctx the parse tree
     */
    void enterExpression(ParserDSL.ExpressionContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#expression}.
     *
     * @param ctx the parse tree
     */
    void exitExpression(ParserDSL.ExpressionContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#ifCondition}.
     *
     * @param ctx the parse tree
     */
    void enterIfCondition(ParserDSL.IfConditionContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#ifCondition}.
     *
     * @param ctx the parse tree
     */
    void exitIfCondition(ParserDSL.IfConditionContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#conditionStatment}.
     *
     * @param ctx the parse tree
     */
    void enterConditionStatment(ParserDSL.ConditionStatmentContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#conditionStatment}.
     *
     * @param ctx the parse tree
     */
    void exitConditionStatment(ParserDSL.ConditionStatmentContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#firstParty}.
     *
     * @param ctx the parse tree
     */
    void enterFirstParty(ParserDSL.FirstPartyContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#firstParty}.
     *
     * @param ctx the parse tree
     */
    void exitFirstParty(ParserDSL.FirstPartyContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#secondParty}.
     *
     * @param ctx the parse tree
     */
    void enterSecondParty(ParserDSL.SecondPartyContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#secondParty}.
     *
     * @param ctx the parse tree
     */
    void exitSecondParty(ParserDSL.SecondPartyContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#forStatment}.
     *
     * @param ctx the parse tree
     */
    void enterForStatment(ParserDSL.ForStatmentContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#forStatment}.
     *
     * @param ctx the parse tree
     */
    void exitForStatment(ParserDSL.ForStatmentContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#blockStatment}.
     *
     * @param ctx the parse tree
     */
    void enterBlockStatment(ParserDSL.BlockStatmentContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#blockStatment}.
     *
     * @param ctx the parse tree
     */
    void exitBlockStatment(ParserDSL.BlockStatmentContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#printStatment}.
     *
     * @param ctx the parse tree
     */
    void enterPrintStatment(ParserDSL.PrintStatmentContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#printStatment}.
     *
     * @param ctx the parse tree
     */
    void exitPrintStatment(ParserDSL.PrintStatmentContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#function}.
     *
     * @param ctx the parse tree
     */
    void enterFunction(ParserDSL.FunctionContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#function}.
     *
     * @param ctx the parse tree
     */
    void exitFunction(ParserDSL.FunctionContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#writeOnFileData}.
     *
     * @param ctx the parse tree
     */
    void enterWriteOnFileData(ParserDSL.WriteOnFileDataContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#writeOnFileData}.
     *
     * @param ctx the parse tree
     */
    void exitWriteOnFileData(ParserDSL.WriteOnFileDataContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#readFromFileData}.
     *
     * @param ctx the parse tree
     */
    void enterReadFromFileData(ParserDSL.ReadFromFileDataContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#readFromFileData}.
     *
     * @param ctx the parse tree
     */
    void exitReadFromFileData(ParserDSL.ReadFromFileDataContext ctx);

    /**
     * Enter a parse tree produced by {@link ParserDSL#parameter}.
     *
     * @param ctx the parse tree
     */
    void enterParameter(ParserDSL.ParameterContext ctx);

    /**
     * Exit a parse tree produced by {@link ParserDSL#parameter}.
     *
     * @param ctx the parse tree
     */
    void exitParameter(ParserDSL.ParameterContext ctx);
}