package ast.Node.Body.Form;

import ast.Node.Body.Form.Attribute.AttributeForm;
import ast.Node.Body.Form.BodyForm.BodyForm;
import ast.Node.ParentNode;
import ast.Node.Scope;

import java.util.ArrayList;
import java.util.Objects;

public class Form extends ParentNode {
    AttributeForm attributeForm;
    ArrayList<BodyForm> bodyForm;
    Scope scope;

    public Form() {
    }

    public Form(AttributeForm attributeForm, ArrayList<BodyForm> bodyForm, Scope scope) {
        this.attributeForm = attributeForm;
        this.bodyForm = bodyForm;
        this.scope = scope;
    }

    public Form(Scope scope) {
        this.scope = scope;
    }

    public Form(AttributeForm attributeForm, ArrayList<BodyForm> bodyForm) {
        this.attributeForm = attributeForm;
        this.bodyForm = bodyForm;
    }

    public AttributeForm getAttributeForm() {
        return attributeForm;
    }

    public void setAttributeForm(AttributeForm attributeForm) {
        this.attributeForm = attributeForm;
    }

    public ArrayList<BodyForm> getBodyForm() {
        return bodyForm;
    }

    public void setBodyForm(ArrayList<BodyForm> bodyForm) {
        this.bodyForm = bodyForm;
    }

    public Scope getScope() {
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Form form)) return false;
        return getAttributeForm().equals(form.getAttributeForm()) && getBodyForm().equals(form.getBodyForm()) && getScope().equals(form.getScope());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getAttributeForm(), getBodyForm(), getScope());
    }

    @Override
    public String toString() {
        return "Form{" +
                "attributeForm=" + attributeForm +
                ", bodyForm=" + bodyForm +
                ", scope=" + scope +
                "}\n";
    }
}
