package ErrorHandling;

import org.antlr.v4.runtime.BaseErrorListener;
import org.antlr.v4.runtime.RecognitionException;
import org.antlr.v4.runtime.Recognizer;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

public class ErrorSyntax extends BaseErrorListener {
    public static final ErrorSyntax INSTANCE = new ErrorSyntax();

    public static void writeErrorsOnFile() {
        BufferedWriter writer = null;
        try {
            writer = new BufferedWriter(new FileWriter("src/ErrorFilesOutput/syntaxError"));
            for (int i = 0; i < StoreError.errorList.size(); i++) {
                try {

                    writer.write(StoreError.errorList.get(i));

                } catch (IOException ioException) {
                    System.out.println(ioException.getMessage());
                }
            }
        } catch (IOException ioException) {
            System.out.println(ioException.getMessage());
        } finally {
            try {
                assert writer != null;
                writer.close();
            } catch (IOException e) {
                System.out.println(e.getMessage());
            }
        }

        System.out.println("you have some syntax error please go to ( src/ErrorFilesOutput/syntaxError ) ");
    }

    @Override
    public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
        String error = "line " + line + ":" + charPositionInLine + " " + msg + "\n";
        StoreError.errorList.add(error);
    }
}
