package ErrorHandling;

import ast.Node.controller.Variable;

import java.util.HashMap;

public class SemanticCheck {
    public static boolean checkIfRedefineVariable(HashMap<String, Object> st_variable, Variable variable) {
        boolean checkRangeIsNotAvailable = true;
        for (int i = 0; i < StoreError.rangeIsAvailableDefine.size(); i++) {

            if (variable.getScope().getStartScope() >= StoreError.rangeIsAvailableDefine.get(i).first &&
                    variable.getScope().getEndScope() < StoreError.rangeIsAvailableDefine.get(i).second) {
                checkRangeIsNotAvailable = false;
                break;
            }
        }
        return st_variable.containsKey(variable.getNameVariable()) && checkRangeIsNotAvailable;
    }

    public static boolean checkIfVariableInConditionIsDefine(HashMap<String, Object> hashMap, Variable variable) {
        return hashMap.containsKey(variable.getNameVariable());
    }

    public static boolean checkIfHaveMoreThanPageSameName(HashMap<String, Object> hashMap, String key) {
        return hashMap.containsKey(key);
    }
}
