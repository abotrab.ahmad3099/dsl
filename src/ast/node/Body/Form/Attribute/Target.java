package ast.Node.Body.Form.Attribute;

import ast.Node.ParentNode;
import ast.Node.Scope;

public class Target extends ParentNode {
    String targetEvent;


    public Target() {
    }

    public Target(String targetEvent, Scope scope) {
        this.targetEvent = targetEvent;
        super.setScope(scope);
    }

    public String getTargetEvent() {
        return targetEvent;
    }

    public void setTargetEvent(String targetEvent) {
        this.targetEvent = targetEvent;
    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }

}
