package ast.Node.Body.Form.BodyForm;

import ast.Node.ParentNode;
import ast.Node.Scope;

public class TextField extends ParentNode {
    String textFieldName;
    String value;

    String hint;

    public TextField() {
    }

    public TextField(String textFieldName, String value, String hint, Scope scope) {
        this.textFieldName = textFieldName;
        this.value = value;
        this.hint = hint;
        super.setScope(scope);
    }

    public TextField(Scope scope) {
        super.setScope(scope);
    }

    public String getTextFieldName() {
        return textFieldName;
    }

    public void setTextFieldName(String textFieldName) {
        this.textFieldName = textFieldName;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }

    public String getHint() {
        return hint;
    }

    public void setHint(String hint) {
        this.hint = hint;
    }

}
