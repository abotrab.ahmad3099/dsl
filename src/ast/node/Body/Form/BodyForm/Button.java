package ast.Node.Body.Form.BodyForm;

import ast.Node.ParentNode;

import java.util.Objects;

public class Button extends ParentNode {
    String nameButton;
    GoTo goTo;
    OnPressed onPressed;

    public Button() {
    }

    public Button(String nameButton, GoTo goTo, OnPressed onPressed) {
        this.nameButton = nameButton;
        this.goTo = goTo;
        this.onPressed = onPressed;

    }

    public String getNameButton() {
        return nameButton;
    }

    public void setNameButton(String nameButton) {
        this.nameButton = nameButton;
    }

    public GoTo getGoTo() {
        return goTo;
    }

    public void setGoTo(GoTo goTo) {
        this.goTo = goTo;
    }

    public OnPressed getOnPressed() {
        return onPressed;
    }

    public void setOnPressed(OnPressed onPressed) {
        this.onPressed = onPressed;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Button button)) return false;
        return getNameButton().equals(button.getNameButton()) && getGoTo().equals(button.getGoTo()) && getOnPressed().equals(button.getOnPressed());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getNameButton(), getGoTo(), getOnPressed());
    }

    @Override
    public String toString() {
        return "Button{" +
                "nameButton='" + nameButton + '\'' +
                ", goTo=" + goTo +
                ", onPressed=" + onPressed +
                '}';
    }
}
