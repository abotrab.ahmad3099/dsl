package codeGeneration;

import symbol_Table.SymbolTable;

import java.io.IOException;

public interface CodeGeneration {
    void codeGeneration(SymbolTable symbolTable) throws IOException;
}
