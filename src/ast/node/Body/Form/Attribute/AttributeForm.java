package ast.Node.Body.Form.Attribute;

import ast.Node.ParentNode;
import ast.Node.Scope;

public class AttributeForm extends ParentNode {
    Action action;
    Target target;
    Method method;
    AutoComplete autoComplete;


    String nameAttribute;

    public AttributeForm(Action action, Target target, Method method, AutoComplete autoComplete, Scope scope, String nameAttribute) {
        this.action = action;
        this.target = target;
        this.method = method;
        this.autoComplete = autoComplete;
        super.setScope(scope);
        this.nameAttribute = nameAttribute;
    }

    public AttributeForm() {
    }

    public AttributeForm(Scope scope) {
        super.setScope(scope);
    }

    public AttributeForm(Action action, Target target, Method method, AutoComplete autoComplete, Scope scope) {
        this.action = action;
        this.target = target;
        this.method = method;
        this.autoComplete = autoComplete;
        super.setScope(scope);
    }

    public AttributeForm(Action action, Target target, Method method, AutoComplete autoComplete) {
        this.action = action;
        this.target = target;
        this.method = method;
        this.autoComplete = autoComplete;
    }

    public Action getAction() {
        return action;
    }

    public void setAction(Action action) {
        this.action = action;
    }

    public Target getTarget() {
        return target;
    }

    public void setTarget(Target target) {
        this.target = target;
    }

    public Method getMethod() {
        return method;
    }

    public void setMethod(Method method) {
        this.method = method;
    }

    public AutoComplete getAutoComplete() {
        return autoComplete;
    }

    public void setAutoComplete(AutoComplete autoComplete) {
        this.autoComplete = autoComplete;
    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }

    public String getNameAttribute() {
        return nameAttribute;
    }

    public void setNameAttribute(String nameAttribute) {
        this.nameAttribute = nameAttribute;
    }


}
