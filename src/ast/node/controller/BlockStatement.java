package ast.Node.controller;

import ast.Node.ParentNode;
import ast.Node.Scope;

public class BlockStatement extends ParentNode {

    Variable variables;
    ForStatement forStatements;
    Function functions;
    IfStatement ifStatements;

    WriteOnFileData writeOnFile;

    Print prints;


    public BlockStatement(Scope scope, Variable variables, ForStatement forStatements, Function functions, IfStatement ifStatements, WriteOnFileData writeOnFile, Print prints) {
        super.setScope(scope);
        this.variables = variables;
        this.forStatements = forStatements;
        this.functions = functions;
        this.ifStatements = ifStatements;
        this.writeOnFile = writeOnFile;
        this.prints = prints;
    }

    public BlockStatement(Scope scope, Variable variables, ForStatement forStatements, Function functions, IfStatement ifStatements, Print prints) {
        super.setScope(scope);
        this.variables = variables;
        this.forStatements = forStatements;
        this.functions = functions;
        this.ifStatements = ifStatements;
        this.prints = prints;
    }

    public BlockStatement() {
    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }

    public Variable getVariables() {
        return variables;
    }

    public void setVariables(Variable variables) {
        this.variables = variables;
    }

    public ForStatement getForStatements() {
        return forStatements;
    }

    public void setForStatements(ForStatement forStatements) {
        this.forStatements = forStatements;
    }

    public Function getFunctions() {
        return functions;
    }

    public void setFunctions(Function functions) {
        this.functions = functions;
    }

    public IfStatement getIfStatements() {
        return ifStatements;
    }

    public void setIfStatements(IfStatement ifStatements) {
        this.ifStatements = ifStatements;
    }

    public Print getPrints() {
        return prints;
    }

    public void setPrints(Print prints) {
        this.prints = prints;
    }

    public WriteOnFileData getWriteOnFile() {
        return writeOnFile;
    }

    public void setWriteOnFile(WriteOnFileData writeOnFile) {
        this.writeOnFile = writeOnFile;
    }

}
