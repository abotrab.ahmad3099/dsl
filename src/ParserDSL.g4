parser grammar ParserDSL;

options {
 tokenVocab = LexerDSL;
// tokenVocab = LexerControllerDSL;
}

doc
    :
    AttributDefault*
    page*
    AttributDefault*
    ;

page :
    AttributDefault* PAGE AttributDefault* OperationDefault AttributDefault*
    PAGEOPEN AttributeInPage* OpenBracket_PAGE AttributeInPage*
    icon*
    AttributeInPage*
    title*
    AttributeInPage*
    attributeInPage
    AttributeInPage*
    CloseBracket_PAGE
    AttributDefault*
    SimiColonPage
    AttributDefault*
    ;
icon: Icon AttributeInPage*  OperationInPage AttributeInPage*
               (
                 (DoubleQuatePage | SingleQuatePage )* AttributeInPage*
                 (NameClass | AttributeInPage | DotIn_page)* AttributeInPage*
                 (DoubleQuatePage | SingleQuatePage )*
               )
      AttributeInPage* OperationInPage;
title:
     TITLE AttributeInPage* OperationInPage AttributeInPage*
     (
        (
          (DoubleQuatePage | SingleQuatePage )* AttributeInPage*
          (NameClass | AttributeInPage )* AttributeInPage*
          (DoubleQuatePage | SingleQuatePage )*
        )
     )
     AttributeInPage* OperationInPage AttributeInPage*;

attributeInPage :
                    navbar*
                    AttributeInPage*
                    body*
                    AttributeInPage*
                    footer*
                    AttributeInPage*
                    controllerRule*
                    AttributeInPage*
                ;

navbar :
      NAVBAR AttributeInNav* OperationInNav AttributeInNav* NAVBAR_inMode AttributeInNav* OpenBracket_NAV AttributeInNav*
        (bodyNavBar*  | styleNavBar)
      AttributeInNav* CloseBracket_NAV  AttributeInNav* SimiColonNav
      ;


body :
    BODY AttributeInBody* OperationInBody AttributeInBody* BodyInMode AttributeInBody* OpenBracket_BODY AttributeInBody*
    (Center* AttributeInBody* OperationInBody AttributeInBody*)*
    (bodyBody* | styleBody*)
    AttributeInBody* CloseBracket_BODY AttributeInBody* SimiColonBody
    ;


footer :
        FOOTER  AttributeInFooter* OperationInFooter AttributeInFooter* FOOTERNINMODE AttributeInFooter* OpenBracket_FOOTER AttributeInFooter*
        bodyFooter* AttributeInFooter* CloseBracket_FOOTER AttributeInFooter* SIMICOLON_inModeFooter
        ;


bodyNavBar
        : TextInNav
        ;
bodyBody
        : form
        ;

bodyFooter
        :TextInFooter
        ;

styleNavBar
        : StyleNavBar
        ;
styleBody
        : StyleBody ColonStyle
        ;

form
    :
      Form AttributeInForm* OperationInForm AttributeInForm* OpenBracketForm
      attributeForm bodyForm*
      CloseBracketForm AttributeInForm* SIMICOLON_inModeForm AttributeInForm*
    ;
attributeForm
            :
                (AttributeInForm*
                action*
                AttributeInForm*
                method*
                AttributeInForm*
                target*
                AttributeInForm*
                autoComplete*
                AttributeInForm*)
            ;
action
    :
     Action
     AttributeInAction*
     EqualAction
     AttributeInAction*
     ( (DoubleQuationAction | SingleQuationAction)*  AttributeInAction* (TextCharAction)*   AttributeInAction* (DoubleQuationAction | SingleQuationAction)* )
     AttributeInAction*
     SimiColonAction
    ;
method
    :
       Method
       AttributeInMethod*
       EqualMethod
       AttributeInMethod*
       ( (DoubleQuateMethod | SingleQuateMethod)* AttributeInMethod* (GET | POST  | PUT)* AttributeInMethod* (DoubleQuateMethod | SingleQuateMethod)* )
       SimicolonMethod
    ;
target
    :
       Target
       AttributeInTarget*
       EqualTarget
       AttributeInTarget*
       ((DoubleQuateTarget | SingleQuateTarget)* AttributeInTarget* (Blank | Self | Parent | Top)* AttributeInTarget* (DoubleQuateTarget | SingleQuateTarget)* )
       AttributeInTarget*
       SimicolonTarget
    ;
autoComplete
    :
       AutoComplete
       AttributeInAuto*
       EqualAuto
       AttributeInAuto*
       ((DoubleQuateAuto | SingleQuateAuto)* AttributeInAuto* (On | Off)* AttributeInAuto* (DoubleQuateAuto | SingleQuateAuto)* )
       AttributeInAuto*
       SimicolonAuto
    ;
bodyForm
    :

        (
            AttributeInForm*
            textFieldRule
            AttributeInForm*
        )
        |
        (
            AttributeInForm*
            password
            AttributeInForm*
        )
        |
        (
            AttributeInForm*
            radio
            AttributeInForm*
        )
        |
        (
            AttributeInForm*
            dateTime
            AttributeInForm*
        )
        |
        (
             AttributeInForm*
             button
             AttributeInForm*
        )
        AttributeInForm*
    ;

button
    :
        FormButton
        AttributButton*
        OperatorInButton
        AttributButton*
        NameButton*
        AttributButton*
        OperatorInButton
        AttributButton*
        onPressedRule
        AttributButton*
        OperatorInButton*
        AttributButton*
        (
             gotoRule*
        )
        AttributButton*
        SimicolonButton
    ;
gotoRule
        :
           GoTo AttributButton* NameButton* AttributButton*
        ;
onPressedRule
        :
            AttributButton* NameButton* AttributButton*
        ;
radio
    :
        RadioButton
        AttributRadio*
        OperatorInRadio
        AttributRadio*
        NameRadio*
        AttributRadio*
        SimicolonRadio
    ;
dateTime
    :
         DateTime
         AttributdateTime*
         OperatorIndateTime*
         NamedateTime*
         AttributdateTime*
         SimicolondateTime
    ;

password
    :
             Password
             Attributpassword*
             OperatorInpassword*
             Namepassword*
             Attributpassword*
             Simicolonpassword
    ;
textFieldRule
      :
           (
                TextFieldSmall
              | TextFieldSmallAndLarge
              | TextFieldLarge
              | TextFieldXLarge
           )
           AttributTextField*
           OperatorInTextField
           NameFieldInput*
           AttributTextField*
           OpenBracketTextField
           AttributTextField*
           hintR*
           AttributTextField*
           value*
           AttributTextField*
           CloseBracketTextField
           SimicolonTextField
      ;
value
    : Value AttributeInValue* EqualValue
      ( (DoubleQuateValue | SingleQuateValue)* (TextCharValue | AttributeInValue)* (DoubleQuateValue | SingleQuateValue)* )
      SimicolonValue
    ;
hintR
    :
      HintText
      AttributHint*
      EqualHint
      AttributHint*
      (TextCharHint | AttributHint)*
      SimiColonHint
    ;


controllerRule
        :
            Controller
            (SpacesOrEndLineOrTab*
            function+
            SpacesOrEndLineOrTab*)*
            EndController
        ;
variableDefine : Text+
                     SpacesOrEndLineOrTab*
                     (
                         Assignment
                         WhiteSpaceSecond*

                         (
                            (DoubleQuateSecond | SingleQuateSecond)+ (TextNameVariable*) (DoubleQuateSecond | SingleQuateSecond)+
                              |
                              (readFromFileData)
                              |
                            (TextNameVariable)+
                              |
                            (expression)
                            |

                         )
                     )*
                     WhiteSpaceSecond*
                 SimicolonSecond*
                 SpacesOrEndLineOrTab*;

expression
        :
           (
             (DoubleQuateSecond | SingleQuateSecond)*
               TextNameVariable+
             (DoubleQuateSecond | SingleQuateSecond)*
              WhiteSpaceSecond*
             (ADD2 | Minus2 | Multipule2 |Divid2)
              WhiteSpaceSecondSecond*

             ( DoubleQuateSecondSecond | SingleQuateSecondSecond)*
              TEXTSECOND+
             ( DoubleQuateSecondSecond | SingleQuateSecondSecond )*

           )+
             SimicolonSecondSecond
        ;



ifCondition :
              IF SpacesOrEndLineOrTab*
              OpenBracketController SpacesOrEndLineOrTab*

               (conditionStatment  SpacesOrEndLineOrTab* (And | Or)* SpacesOrEndLineOrTab*)+

              SpacesOrEndLineOrTab* CloseBracketController
              SpacesOrEndLineOrTab*
              OpenQurlyBracket SpacesOrEndLineOrTab* blockStatment* SpacesOrEndLineOrTab* CloseQurlyBracket SpacesOrEndLineOrTab*
              (ELSE SpacesOrEndLineOrTab*
              OpenQurlyBracket SpacesOrEndLineOrTab* blockStatment* SpacesOrEndLineOrTab* CloseQurlyBracket SpacesOrEndLineOrTab*)*
            ;


conditionStatment
        :
            firstParty
            SpacesOrEndLineOrTab*
            (EqualController | EqualValueAndType)
            SpacesOrEndLineOrTab*
            secondParty
            SpacesOrEndLineOrTab*
        ;
firstParty
    :
       (DoubleQuateController | SingleQuateController)*
        Text* SpacesOrEndLineOrTab*
       (DoubleQuateController | SingleQuateController)*
        SpacesOrEndLineOrTab*
    ;
secondParty
    :
           (DoubleQuateController | SingleQuateController)*
            Text* SpacesOrEndLineOrTab*
           (DoubleQuateController | SingleQuateController)*
           SpacesOrEndLineOrTab*
    ;


forStatment
          :
            For SpacesOrEndLineOrTab* Text* SpacesOrEndLineOrTab* IN
            SpacesOrEndLineOrTab* OpenBracketController SpacesOrEndLineOrTab*
                (Text)* SpacesOrEndLineOrTab* OperatorNeeded SpacesOrEndLineOrTab* (Text)*
            SpacesOrEndLineOrTab* CloseBracketController  SpacesOrEndLineOrTab*

            OpenQurlyBracket
            SpacesOrEndLineOrTab*
            blockStatment*
            CloseQurlyBracket
            SpacesOrEndLineOrTab*
          ;

blockStatment
            :
                printStatment
                |
                writeOnFileData
                |
                variableDefine
                |
                forStatment
                |
                ifCondition
            ;

printStatment
            :
                Print SpacesOrEndLineOrTab*
                OpenBracketController SpacesOrEndLineOrTab*
                (DoubleQuateController | SingleQuateController )*
                Text*
                (DoubleQuateController | SingleQuateController )*
                CloseBracketController SpacesOrEndLineOrTab*
            ;

function
        :
            Function
                SpacesOrEndLineOrTab*
                Text+
                OpenBracketController
                    SpacesOrEndLineOrTab*

                    parameter*

                    SpacesOrEndLineOrTab*

                CloseBracketController
                SpacesOrEndLineOrTab*

                OpenQurlyBracket

                SpacesOrEndLineOrTab*

                blockStatment*

                SpacesOrEndLineOrTab*

            CloseQurlyBracket
            SpacesOrEndLineOrTab*
        ;

writeOnFileData : WriteFile Spaceswritefile* Operatorwritefile Spaceswritefile*
                    (DoubleQuatewritefile | SingleQuatewritefile)*  Spaceswritefile* Namewritefile* Spaceswritefile* (DoubleQuatewritefile | SingleQuatewritefile)*  Spaceswritefile* SimiColonwritefile SpacesOrEndLineOrTab*;

readFromFileData : ReadFile Spacesreadfile* Operatorreadfile Spacesreadfile*
                                       (DoubleQuatereadfile | SingleQuatereadfile)*  Spacesreadfile* Namereadfile* Spacesreadfile*      (DoubleQuatereadfile | SingleQuatereadfile)*  Spacesreadfile* SimiColonreadfile SpacesOrEndLineOrTab*;
parameter :
                (DoubleQuateController | SingleQuateController)*
                 Text+
                (DoubleQuateController | SingleQuateController)*
                SpacesOrEndLineOrTab*
                OperatorNeeded*
           ;
