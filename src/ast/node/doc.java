package ast.Node;

import ast.Node.Page.Page;

import java.util.ArrayList;
import java.util.Objects;

public class Doc extends ParentNode {
    ArrayList<Page> pages = new ArrayList<>();

    public Doc() {
        pages = new ArrayList<>();
        super.setScope(new Scope());
    }

    public Doc(ArrayList<Page> pages, Scope scope) {
        this.pages = pages;
        super.scope = scope;
    }

    public Doc(ArrayList<Page> pages) {
        this.pages = pages;
    }

    public Doc(Scope scope) {
        super.scope = scope;
    }

    public ArrayList<Page> getPages() {
        return pages;
    }

    public void setPages(ArrayList<Page> pages) {
        this.pages = pages;
    }

    public Scope getScope() {
        return scope;
    }

    public void setScope(Scope scope) {
        super.scope = scope;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Doc doc)) return false;
        return getPages().equals(doc.getPages()) && getScope().equals(doc.getScope());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getPages(), getScope());
    }

    @Override
    public String toString() {
        return " Doc { \n" +
                "pages = " + pages +
                "\n, scope=" + scope +
                "}\n";
    }
}
