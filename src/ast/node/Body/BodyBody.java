package ast.Node.Body;

import ast.Node.Body.Form.Form;
import ast.Node.ParentNode;
import ast.Node.Scope;

public class BodyBody extends ParentNode {
    String nameBodyBody;
    Form form;

    public BodyBody() {

    }

    public BodyBody(String nameBodyBody, Form form) {
        this.nameBodyBody = nameBodyBody;
        this.form = form;
    }

    public BodyBody(String nameBodyBody, Form form, Scope scope) {
        this.nameBodyBody = nameBodyBody;
        this.form = form;
        super.setScope(scope);
    }

    public String getNameBodyBody() {
        return nameBodyBody;
    }

    public void setNameBodyBody(String nameBodyBody) {
        this.nameBodyBody = nameBodyBody;
    }

    public Form getForm() {
        return form;
    }

    public void setForm(Form form) {
        this.form = form;
    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }

}
