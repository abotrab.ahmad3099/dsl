package ast.Node.Page;

import ast.Node.ParentNode;
import ast.Node.Scope;

public class Icon extends ParentNode {

    String pathToIcon;

    public Icon() {
    }

    public Icon(Scope scope, String pathToIcon) {
        super.setScope(scope);
        this.pathToIcon = pathToIcon;
    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }

    public String getPathToIcon() {
        return pathToIcon;
    }

    public void setPathToIcon(String pathToIcon) {
        this.pathToIcon = pathToIcon;
    }

}
