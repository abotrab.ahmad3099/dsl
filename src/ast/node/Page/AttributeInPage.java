package ast.Node.Page;

import ast.Node.Body.Body;
import ast.Node.ParentNode;
import ast.Node.Scope;
import ast.Node.controller.Function;

import java.util.ArrayList;


public class AttributeInPage extends ParentNode {

    Body body;
    ArrayList<Function> functions;


    public AttributeInPage() {

    }

    public AttributeInPage(Body body) {

        this.body = body;

    }

    public AttributeInPage(Body body, Scope scope) {

        this.body = body;

        super.setScope(scope);
    }

    public AttributeInPage(Body body, ArrayList<Function> functions, Scope scope) {

        this.body = body;

        this.functions = functions;
        super.setScope(scope);
    }

    public AttributeInPage(Scope scope) {
        super.setScope(scope);
    }


    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }


    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }

    public ArrayList<Function> getFunctions() {
        return functions;
    }

    public void setFunctions(ArrayList<Function> functions) {
        this.functions = functions;
    }


}
