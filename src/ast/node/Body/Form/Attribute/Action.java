package ast.Node.Body.Form.Attribute;

import ast.Node.ParentNode;
import ast.Node.Scope;

public class Action extends ParentNode {
    String pathToMakeAction;


    public Action() {
    }

    public Action(String pathToMakeAction, Scope scope) {
        this.pathToMakeAction = pathToMakeAction;
        super.setScope(scope);
    }

    public String getPathToMakeAction() {
        return pathToMakeAction;
    }

    public void setPathToMakeAction(String pathToMakeAction) {
        this.pathToMakeAction = pathToMakeAction;
    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }

}
