package symbol_Table;

import java.util.HashMap;
import java.util.Objects;

public class SymbolTable {
    HashMap<String, Object> symbols;
    HashMap<String, Object> symbolTableController;

    HashMap<String, Object> st_variable;

    public SymbolTable() {
        symbols = new HashMap<>();
        st_variable = new HashMap<>();
        symbolTableController = new HashMap<>();

    }

    public HashMap<String, Object> getSymbols() {
        return symbols;
    }

    public void setSymbols(HashMap<String, Object> symbols) {
        this.symbols = symbols;
    }

    public HashMap<String, Object> getSymbolTableController() {
        return symbolTableController;
    }

    public void setSymbolTableController(HashMap<String, Object> symbolTableController) {
        this.symbolTableController = symbolTableController;
    }

    public void putNewSymbolAfterValidation(String key, Object object) {
        symbols.put(key, object);
    }

    public HashMap<String, Object> getSt_variable() {
        return st_variable;
    }

    public void setSt_variable(HashMap<String, Object> st_variable) {
        this.st_variable = st_variable;
    }

    public void putNewSymbolAfterValidationIn_Controller(String key, Object object) {
        symbolTableController.put(key, object);
    }

    public void putOnVariableST(String key, Object object) {
        st_variable.put(key, object);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof SymbolTable that)) return false;
        return getSymbols().equals(that.getSymbols());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getSymbols());
    }

    @Override
    public String toString() {
        return "SymbolTable { \n" +
                "symbols = \n" + symbols +
                "}\n";
    }
}
