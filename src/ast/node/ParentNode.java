package ast.Node;

public abstract class ParentNode {
    Scope scope;

    public ParentNode() {
    }

    public ParentNode(Scope scope) {
        this.scope = scope;
    }

    public Scope getScope() {
        return scope;
    }

    public void setScope(Scope scope) {
        this.scope = scope;
    }
}
