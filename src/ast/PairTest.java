package ast;

public class PairTest implements Comparable {
    public String first;
    public double second;


    public PairTest(String first, double second) {
        this.first = first;
        this.second = second;
    }

    @Override
    public int compareTo(Object o) {
        PairTest pairTest = (PairTest) o;
        return Double.compare(second, pairTest.second);

    }

    @Override
    public String toString() {
        return "PairTest{" +
                "first='" + first + "'\n''" +
                ", second=" + second +
                "}\n";
    }
}
