package ast.visitor;

import ErrorHandling.SemanticCheck;
import ErrorHandling.StoreError;
import ast.Node.Body.Body;
import ast.Node.Body.BodyBody;
import ast.Node.Body.Form.Attribute.*;
import ast.Node.Body.Form.BodyForm.*;
import ast.Node.Body.Form.Form;
import ast.Node.Doc;
import ast.Node.Page.AttributeInPage;
import ast.Node.Page.Icon;
import ast.Node.Page.Page;
import ast.Node.Page.Title;
import ast.Node.Scope;
import ast.Node.controller.*;
import generated.ParserDSL;
import generated.ParserDSLBaseVisitor;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.RuleNode;
import org.antlr.v4.runtime.tree.TerminalNode;
import symbol_Table.SymbolTable;

import java.util.ArrayList;
import java.util.List;

import static ErrorHandling.StoreError.counter;
import static codeGeneration.GenerateController.writeOnFile;


public class BaseVisitor extends ParserDSLBaseVisitor {

    SymbolTable symbolTable = new SymbolTable();

    public static void writeSemanticError(String textError, Variable variable) {

        System.out.println("Please you have logical error please read file semantic error file");
        String fullPath = "src/ErrorFilesOutput/semantic error";
        StringBuilder mainString = new StringBuilder();
        String text = textError + "( " + variable.getNameVariable() + " ) " + " in" +
                "line : " + variable.getScope().getStartScope() + " : " + variable.getScope().getEndScope() + " \n";
        mainString.append(text);
        writeOnFile(fullPath, mainString);
    }

    public static void writeSemanticError(Page page) {
        System.out.println("Please you have logical error please read file semantic error file");
        String fullPath = "src/ErrorFilesOutput/semantic error";
        StringBuilder mainString = new StringBuilder();
        String textError = "Warning : You have Duplicated to name page ( " + page.getNamePage() + " ) " + " in" +
                "line : " + page.getScope().getStartScope() + " : " + page.getScope().getEndScope() + " \n";
        mainString.append(textError);
        writeOnFile(fullPath, mainString);
    }

    @Override
    public Object visitWriteOnFileData(ParserDSL.WriteOnFileDataContext ctx) {

        WriteOnFileData writeOnFile = new WriteOnFileData();
        writeOnFile.setScope(new Scope(ctx.start.getLine(), ctx.stop.getLine()));
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < ctx.Namewritefile().size(); i++) {
            stringBuilder.append(ctx.Namewritefile().get(i));
        }
        if (!ctx.DoubleQuatewritefile().isEmpty() || !ctx.SingleQuatewritefile().isEmpty()) {

            writeOnFile.setData(String.valueOf(stringBuilder));
        } else {
            Variable variable = new Variable(writeOnFile.getScope(), String.valueOf(stringBuilder), new Object());

            boolean check = SemanticCheck.checkIfVariableInConditionIsDefine(symbolTable.getSt_variable(), variable);
            if (!check) {
                writeSemanticError("Warning : you try print variable not declare", variable);
                StoreError.counter++;
            } else {
                Variable temp = (Variable) symbolTable.getSt_variable().get(String.valueOf(stringBuilder));
                if (temp.isParameter()) {
                    String ss = "_POST['" + temp.getNameVariable() + "']";
                    variable = new Variable(variable.getScope(), ss, new Object(), true);
                }
                writeOnFile.setData(variable);
            }

        }
        return writeOnFile;
    }

    @Override
    public Object visitDoc(ParserDSL.DocContext ctx) {
        Doc doc = new Doc();
        ArrayList<Page> pages = new ArrayList<>();

        for (int i = 0; i < ctx.page().size(); i++) {
            Page page = (Page) visitPage(ctx.page(i));
            pages.add(page);
        }
        doc.setPages(pages);
        Scope scope = new Scope(ctx.start.getLine(), ctx.stop.getLine());
        doc.setScope(scope);
        symbolTable.putNewSymbolAfterValidation(Doc.class.getSimpleName(), doc);
        return symbolTable;
    }

    @Override
    public Object visitTitle(ParserDSL.TitleContext ctx) {
        Title title = new Title();
        StringBuilder textTitle = new StringBuilder();
        for (int i = 0; i < ctx.NameClass().size(); i++) {
            textTitle.append(ctx.NameClass().get(i).getText());
        }
        String x = String.valueOf(textTitle);
        title.setTitleString(x);
        Scope scope = new Scope(ctx.start.getLine(), ctx.stop.getLine());
        title.setScope(scope);
        symbolTable.putNewSymbolAfterValidation(Title.class.getSimpleName(), title);
        return title;
    }

    @Override
    public Object visitIcon(ParserDSL.IconContext ctx) {
        Icon icon = new Icon();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < ctx.NameClass().size(); i++) {
            stringBuilder.append(ctx.NameClass().get(i));
        }
        icon.setPathToIcon(String.valueOf(stringBuilder));
        icon.setScope(new Scope(ctx.start.getLine(), ctx.stop.getLine()));
        return icon;
    }

    @Override
    public Object visitExpression(ParserDSL.ExpressionContext ctx) {
        Expression expression = new Expression();
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < ctx.TextNameVariable().size(); i++) {
            stringBuilder.append(ctx.TextNameVariable().get(i));
        }


        if (!ctx.DoubleQuateSecond().isEmpty() || !ctx.SingleQuateSecond().isEmpty()) {
            expression.setFirst(String.valueOf(stringBuilder));
        } else {
            String s = String.valueOf(stringBuilder);
            char[] ii = s.toCharArray();
            if (ii[0] >= '0' && ii[0] <= '9') {
                expression.setFirst(s);
            } else {
                expression.setFirst(new Variable(new Scope(ctx.start.getLine(), ctx.stop.getLine()), String.valueOf(stringBuilder), new Object()));
            }
        }
        stringBuilder = new StringBuilder();

        if (!ctx.ADD2().isEmpty())
            expression.setOperator(ctx.ADD2().get(0).getText());
        if (!ctx.Minus2().isEmpty())
            expression.setOperator(ctx.Minus2().get(0).getText());
        if (!ctx.Divid2().isEmpty())
            expression.setOperator(ctx.Divid2().get(0).getText());
        if (!ctx.Multipule2().isEmpty())
            expression.setOperator(ctx.Multipule2().get(0).getText());


        for (int i = 0; i < ctx.TEXTSECOND().size(); i++) {
            stringBuilder.append(ctx.TEXTSECOND().get(i));
        }
        if (!ctx.DoubleQuateSecondSecond().isEmpty() || !ctx.SingleQuateSecondSecond().isEmpty()) {
            expression.setSecond(String.valueOf(stringBuilder));
        } else {
            String s = String.valueOf(stringBuilder);
            char[] ii = s.toCharArray();
            if (ii[0] >= '0' && ii[0] <= '9') {
                expression.setSecond(s);
            } else {
                expression.setSecond(new Variable(new Scope(ctx.start.getLine(), ctx.stop.getLine()), String.valueOf(stringBuilder), new Object()));
            }
        }
        return expression;
    }

    @Override
    public Object visitAttributeInPage(ParserDSL.AttributeInPageContext ctx) {
        AttributeInPage attributeInPage = new AttributeInPage();
        Scope scope = new Scope(ctx.start.getLine(), ctx.stop.getLine());


        if (!ctx.body().isEmpty())
            attributeInPage.setBody((Body) visitBody(ctx.body().get(0)));


        if (!ctx.controllerRule().isEmpty())
            for (int i = 0; i < ctx.controllerRule().size(); i++) {

                attributeInPage.setFunctions((ArrayList<Function>) visitControllerRule(ctx.controllerRule().get(i)));
            }

        attributeInPage.setScope(scope);
        symbolTable.putNewSymbolAfterValidation(AttributeForm.class.getSimpleName(), attributeInPage);
        return attributeInPage;
    }

//    @Override
//    public Object visitNavbar(ParserDSL.NavbarContext ctx) {
//        NavBar navBar = new NavBar();
//        Scope scope = new Scope(ctx.start.getLine(), ctx.stop.getLine());
//        if (!ctx.bodyNavBar().isEmpty())
//            navBar.setBodyNavBar((BodyNavBar) visitBodyNavBar(ctx.bodyNavBar().get(0)));
//
//        navBar.setNameNavBar("navbar");
//        navBar.setScope(scope);
//        symbolTable.putNewSymbolAfterValidation(NavBar.class.getSimpleName(), navBar);
//        return navBar;
//    }

    @Override
    public Object visitBody(ParserDSL.BodyContext ctx) {
        Scope scope = new Scope(ctx.start.getLine(), ctx.stop.getLine());
        Body body = new Body();
        body.setScope(scope);
        body.setNameBody("Container-body");
        if (!ctx.Center().isEmpty()) {
            body.setCenter("center");
        }
        if (!ctx.bodyBody().isEmpty()) {
            body.setBodyBody((BodyBody) visitBodyBody(ctx.bodyBody().get(0)));
        }

        symbolTable.putNewSymbolAfterValidation(Body.class.getSimpleName(), body);
        return body;
    }

//    @Override
//    public Object visitFooter(ParserDSL.FooterContext ctx) {
//        Footer footer = new Footer();
//        Scope scope = new Scope(ctx.start.getLine(), ctx.stop.getLine());
//        footer.setScope(scope);
//        if (!ctx.bodyFooter().isEmpty())
//            footer.setBodyFooter((BodyFooter) visitBodyFooter(ctx.bodyFooter().get(0)));
//
//        footer.setNameFooter("footer");
//        symbolTable.putNewSymbolAfterValidation(Footer.class.getSimpleName(), footer);
//        return footer;
//    }

//    @Override
//    public Object visitBodyNavBar(ParserDSL.BodyNavBarContext ctx) {
//        return super.visitBodyNavBar(ctx);
//    }

    @Override
    public Object visitBodyBody(ParserDSL.BodyBodyContext ctx) {
        Scope scope = new Scope(ctx.start.getLine(), ctx.stop.getLine());
        BodyBody bodyBody = new BodyBody();
        bodyBody.setScope(scope);
        bodyBody.setNameBodyBody("Body_Body");
        bodyBody.setForm((Form) visitForm(ctx.form()));
        symbolTable.putNewSymbolAfterValidation(BodyBody.class.getSimpleName(), bodyBody);
        return bodyBody;
    }

    @Override
    public Object visitBodyFooter(ParserDSL.BodyFooterContext ctx) {

        return super.visitBodyFooter(ctx);
    }

    @Override
    public Object visitAttributeForm(ParserDSL.AttributeFormContext ctx) {
        AttributeForm attributeForm = new AttributeForm();
        Scope scope = new Scope(ctx.start.getLine(), ctx.stop.getLine());
        attributeForm.setScope(scope);
        if (!ctx.action().isEmpty())
            attributeForm.setAction((Action) visitAction(ctx.action().get(0)));

        if (!ctx.method().isEmpty())
            attributeForm.setMethod((Method) visitMethod(ctx.method().get(0)));
        else {
            Method method = new Method("post", new Scope(ctx.start.getLine(), ctx.stop.getLine()));
            attributeForm.setMethod(method);
            symbolTable.putNewSymbolAfterValidation(Method.class.getSimpleName(), method);
        }

        if (!ctx.target().isEmpty())
            attributeForm.setTarget((Target) visitTarget(ctx.target().get(0)));

        if (!ctx.autoComplete().isEmpty())
            attributeForm.setAutoComplete((AutoComplete) visitAutoComplete(ctx.autoComplete().get(0)));

        attributeForm.setNameAttribute(attributeForm.getClass().getSimpleName());
        symbolTable.putNewSymbolAfterValidation(AttributeForm.class.getSimpleName(), attributeForm);
        return attributeForm;
    }


    @Override
    public Object visitStyleNavBar(ParserDSL.StyleNavBarContext ctx) {
        return super.visitStyleNavBar(ctx);
    }

    @Override
    public Object visitStyleBody(ParserDSL.StyleBodyContext ctx) {
        return super.visitStyleBody(ctx);
    }

    @Override
    public Object visitForm(ParserDSL.FormContext ctx) {
        Scope scope = new Scope(ctx.start.getLine(), ctx.stop.getLine());
        Form form = new Form();
        form.setScope(scope);
        ArrayList<BodyForm> bodyForms = new ArrayList<>();
        for (int i = 0; i < ctx.bodyForm().size(); i++) {
            bodyForms.add((BodyForm) visitBodyForm(ctx.bodyForm().get(i)));
        }
        form.setBodyForm(bodyForms);
        form.setAttributeForm((AttributeForm) visitAttributeForm(ctx.attributeForm()));
        symbolTable.putNewSymbolAfterValidation(Form.class.getSimpleName(), form);
        return form;
    }


    @Override
    public Object visitAction(ParserDSL.ActionContext ctx) {
        Action action = new Action();
        Scope scope = new Scope(ctx.start.getLine(), ctx.stop.getLine());
        StringBuilder pathInAction = new StringBuilder();
        for (int i = 0; i < ctx.TextCharAction().size(); i++) {
            pathInAction.append(ctx.TextCharAction().get(i));
        }

        action.setPathToMakeAction(String.valueOf(pathInAction));
        action.setScope(scope);
        symbolTable.putNewSymbolAfterValidation(Action.class.getSimpleName(), action);
        return action;
    }

    @Override
    public Object visitMethod(ParserDSL.MethodContext ctx) {
        Method method = new Method();
        Scope scope = new Scope(ctx.start.getLine(), ctx.stop.getLine());

        if (!ctx.POST().isEmpty())
            method.setMethodType(ctx.POST().get(0).getText());

        if (!ctx.PUT().isEmpty())
            method.setMethodType(ctx.PUT().get(0).getText());

        if (!ctx.GET().isEmpty())
            method.setMethodType(ctx.GET().get(0).getText());
        method.setScope(scope);
        symbolTable.putNewSymbolAfterValidation(Method.class.getSimpleName(), method);
        return method;
    }

    @Override
    public Object visitTarget(ParserDSL.TargetContext ctx) {
        Target target = new Target();
        Scope scope = new Scope(ctx.start.getLine(), ctx.stop.getLine());
        target.setScope(scope);
        if (!ctx.Blank().isEmpty())
            target.setTargetEvent(ctx.Blank().get(0).getText());
        if (!ctx.Parent().isEmpty())
            target.setTargetEvent(ctx.Parent().get(0).getText());
        if (!ctx.Self().isEmpty())
            target.setTargetEvent(ctx.Self().get(0).getText());
        if (!ctx.Top().isEmpty())
            target.setTargetEvent(ctx.Top().get(0).getText());
        symbolTable.putNewSymbolAfterValidation(Target.class.getSimpleName(), target);
        return target;
    }

    @Override
    public Object visitAutoComplete(ParserDSL.AutoCompleteContext ctx) {
        AutoComplete autoComplete = new AutoComplete();
        Scope scope = new Scope(ctx.start.getLine(), ctx.stop.getLine());
        autoComplete.setScope(scope);
        if (!ctx.On().isEmpty())
            autoComplete.setState(ctx.On().get(0).getText());
        if (!ctx.Off().isEmpty())
            autoComplete.setState(ctx.Off().get(0).getText());
        symbolTable.putNewSymbolAfterValidation(AutoComplete.class.getSimpleName(), autoComplete);
        return autoComplete;
    }

    @Override
    public Object visitBodyForm(ParserDSL.BodyFormContext ctx) {
        BodyForm bodyForm = new BodyForm();
        Scope scope = new Scope(ctx.start.getLine(), ctx.stop.getLine());
        bodyForm.setScope(scope);
        bodyForm.setNameBodyForm("body_form");
        if (ctx.textFieldRule() != null)
            bodyForm.setTextField((TextField) visitTextFieldRule(ctx.textFieldRule()));
        if (ctx.button() != null)
            bodyForm.setButton((Button) visitButton(ctx.button()));
        if (ctx.password() != null)
            bodyForm.setPasswordField((PasswordField) visitPassword(ctx.password()));
        if (ctx.radio() != null)
            bodyForm.setRadioButton((RadioButton) visitRadio(ctx.radio()));
        if (ctx.dateTime() != null)
            bodyForm.setDateTimeField((DateTimeField) visitDateTime(ctx.dateTime()));
        symbolTable.putNewSymbolAfterValidation(BodyForm.class.getSimpleName(), bodyForm);
        return bodyForm;
    }

    @Override
    public Object visitButton(ParserDSL.ButtonContext ctx) {
        Button button = new Button();
        StringBuilder nameButton = new StringBuilder();
        for (int i = 0; i < ctx.NameButton().size(); i++) {
            nameButton.append(ctx.NameButton().get(i));
        }
        button.setNameButton(String.valueOf(nameButton));
        if (!ctx.gotoRule().isEmpty())
            button.setGoTo((GoTo) visitGotoRule(ctx.gotoRule().get(0)));
        if (!ctx.onPressedRule().isEmpty())
            button.setOnPressed((OnPressed) visitOnPressedRule(ctx.onPressedRule()));
        return button;
    }

    @Override
    public Object visitGotoRule(ParserDSL.GotoRuleContext ctx) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < ctx.NameButton().size(); i++) {
            stringBuilder.append(ctx.NameButton().get(i));
        }
        return new GoTo(new Scope(ctx.start.getLine(), ctx.stop.getLine()), String.valueOf(stringBuilder));
    }

    @Override
    public Object visitOnPressedRule(ParserDSL.OnPressedRuleContext ctx) {
        StringBuilder nameFileOrFunctionMakeEvent = new StringBuilder();
        for (int i = 0; i < ctx.NameButton().size(); i++) {
            nameFileOrFunctionMakeEvent.append(ctx.NameButton().get(i));
        }
        String event = "formaction";

        return new OnPressed(new Scope(ctx.start.getLine(), ctx.stop.getLine()), event, String.valueOf(nameFileOrFunctionMakeEvent));
    }

    @Override
    public Object visitTextFieldRule(ParserDSL.TextFieldRuleContext ctx) {
        TextField textField = new TextField();
        StringBuilder strTextField = new StringBuilder();
        if (!ctx.NameFieldInput().isEmpty()) {
            for (int i = 0; i < ctx.NameFieldInput().size(); i++) {
                strTextField.append(ctx.NameFieldInput().get(i));
            }
            textField.setTextFieldName(String.valueOf(strTextField));
        } else {
            textField.setTextFieldName("textField_1");
        }
        if (!ctx.value().isEmpty())
            textField.setValue((String) visitValue(ctx.value().get(0)));
        if (!ctx.hintR().isEmpty())
            textField.setHint((String) visitHintR(ctx.hintR().get(0)));
        return textField;
    }

    @Override
    public Object visitValue(ParserDSL.ValueContext ctx) {
        StringBuilder strTextField = new StringBuilder();
        for (int i = 0; i < ctx.TextCharValue().size(); i++) {
            strTextField.append(ctx.TextCharValue().get(i));
        }
        return String.valueOf(strTextField);
    }

    @Override
    public Object visitHintR(ParserDSL.HintRContext ctx) {
        StringBuilder strTextField = new StringBuilder();
        for (int i = 0; i < ctx.TextCharHint().size(); i++) {
            strTextField.append(ctx.TextCharHint().get(i));
        }
        return String.valueOf(strTextField);
    }

    @Override
    public Object visitRadio(ParserDSL.RadioContext ctx) {
        RadioButton radioButton = new RadioButton();
        StringBuilder nameRadioButton = new StringBuilder();
        for (int i = 0; i < ctx.NameRadio().size(); i++) {
            nameRadioButton.append(ctx.NameRadio().get(i));
        }
        radioButton.setNameRadioButton(String.valueOf(nameRadioButton));
        radioButton.setScope(new Scope(ctx.start.getLine(), ctx.stop.getLine()));
        return radioButton;
    }

    @Override
    public Object visitDateTime(ParserDSL.DateTimeContext ctx) {
        DateTimeField dateTimeField = new DateTimeField();
        StringBuilder nameDateTimeField = new StringBuilder();
        for (int i = 0; i < ctx.NamedateTime().size(); i++) {
            nameDateTimeField.append(ctx.NamedateTime().get(i));
        }
        dateTimeField.setNameDateTimeField(String.valueOf(nameDateTimeField));
        dateTimeField.setScope(new Scope(ctx.start.getLine(), ctx.stop.getLine()));
        return dateTimeField;
    }

    @Override
    public Object visitPassword(ParserDSL.PasswordContext ctx) {
        PasswordField passwordField = new PasswordField();
        StringBuilder namePasswordField = new StringBuilder();
        for (int i = 0; i < ctx.Namepassword().size(); i++) {
            namePasswordField.append(ctx.Namepassword().get(i));
        }
        passwordField.setNameField(String.valueOf(namePasswordField));
        passwordField.setScope(new Scope(ctx.start.getLine(), ctx.stop.getLine()));
        return passwordField;
    }

    @Override
    public Object visitFunction(ParserDSL.FunctionContext ctx) {
        Function function = new Function();
        function.setScope(new Scope(ctx.start.getLine(), ctx.stop.getLine()));
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < ctx.Text().size(); i++) {
            stringBuilder.append(ctx.Text().get(i));
        }
        function.setNameFunction(String.valueOf(stringBuilder));

        ArrayList<Object> parameterArray = new ArrayList<>();
        for (int i = 0; i < ctx.parameter().size(); i++) {
            parameterArray.add(visitParameter(ctx.parameter().get(i)));
        }
        function.setParameter(parameterArray);

        function.setBlockStatement(visitBlockStatement(ctx.blockStatment()));
        return function;
    }

    @Override
    public Object visitParameter(ParserDSL.ParameterContext ctx) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < ctx.Text().size(); i++) {
            stringBuilder.append(ctx.Text().get(i));
        }
        if (!ctx.DoubleQuateController().isEmpty() || !ctx.SingleQuateController().isEmpty())
            return String.valueOf(stringBuilder);
        else {

            Variable variable = new Variable(new Scope(ctx.start.getLine(), ctx.stop.getLine()), String.valueOf(stringBuilder), new Object(), true);
            symbolTable.getSt_variable().put(variable.getNameVariable(), variable);
            return variable;
        }

    }

    @Override
    public Object visitForStatment(ParserDSL.ForStatmentContext ctx) {
        return super.visitForStatment(ctx);
    }

    @Override
    public Object visitBlockStatment(ParserDSL.BlockStatmentContext ctx) {
        BlockStatement blockStatement = new BlockStatement();
        blockStatement.setScope(new Scope(ctx.start.getLine(), ctx.stop.getLine()));

        /// ------ Todo ---- done

        if (ctx.ifCondition() != null)
            blockStatement.setIfStatements(((IfStatement) visitIfCondition(ctx.ifCondition())));

        ///// ------ Todo ---- waiting
        if (ctx.forStatment() != null)
            blockStatement.setForStatements((ForStatement) visitForStatment(ctx.forStatment()));

        // ------ Todo ---- is done work and generated
        if (ctx.variableDefine() != null)
            blockStatement.setVariables((Variable) visitVariableDefine(ctx.variableDefine()));

        // ------ Todo ---- is done work and generated
        if (ctx.printStatment() != null)
            blockStatement.setPrints((Print) visitPrintStatment(ctx.printStatment()));

        if (ctx.writeOnFileData() != null) {

            blockStatement.setWriteOnFile((WriteOnFileData) visitWriteOnFileData(ctx.writeOnFileData()));
        }


        return blockStatement;
    }

    @Override
    public Object visitPrintStatment(ParserDSL.PrintStatmentContext ctx) {
        Print print = new Print();
        print.setScope(new Scope(ctx.start.getLine(), ctx.stop.getLine()));
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < ctx.Text().size(); i++) {
            stringBuilder.append(ctx.Text().get(i));
        }
        if (!ctx.DoubleQuateController().isEmpty() || !ctx.SingleQuateController().isEmpty())
            print.setTextPrint(String.valueOf(stringBuilder));
        else {
            Variable variable = new Variable(new Scope(ctx.start.getLine(), ctx.stop.getLine()), String.valueOf(stringBuilder), new Object());
            boolean check = SemanticCheck.checkIfVariableInConditionIsDefine(symbolTable.getSt_variable(), variable);
            if (check) {
                Variable temp = (Variable) symbolTable.getSt_variable().get(String.valueOf(stringBuilder));
                if (temp.isParameter()) {
                    String ss = "_POST['" + temp.getNameVariable() + "']";
                    variable = new Variable(variable.getScope(), ss, new Object(), true);
                }
                print.setTextPrint(variable);
            } else {
                writeSemanticError("Warning : you try print variable not declare", variable);
                StoreError.counter++;
            }
        }
        return print;
    }

    @Override
    public Object visitPage(ParserDSL.PageContext ctx) {
        Scope scope = new Scope(ctx.start.getLine(), ctx.stop.getLine());
        Page page = new Page();

        if (!ctx.title().isEmpty()) {
            page.setTitle((Title) visitTitle(ctx.title().get(0)));
        }
        if (ctx.title().isEmpty()) {
            Title title = new Title();
            title.setScope(new Scope(ctx.start.getLine() + 1, ctx.stop.getLine() - 1));

            title.setTitleString("Default Title");
            page.setTitle(title);
            symbolTable.putNewSymbolAfterValidation(Title.class.getSimpleName(), title);
        }

        if (!ctx.icon().isEmpty())
            page.setIcon((Icon) visitIcon(ctx.icon().get(0)));
        else
            page.setIcon(new Icon(new Scope(ctx.start.getLine() + 1, ctx.stop.getLine() - 1), "profile"));

        page.setAttributeInPage((AttributeInPage) visitAttributeInPage(ctx.attributeInPage()));
        page.setScope(scope);
        page.setNamePage(ctx.PAGEOPEN().getText());

        StoreError.haveSemanticError =
                SemanticCheck.checkIfHaveMoreThanPageSameName(symbolTable.getSymbols(), page.getNamePage());
        if (StoreError.haveSemanticError) {
            writeSemanticError(page);
            StoreError.counter++;
        } else {
            symbolTable.putNewSymbolAfterValidation(page.getNamePage(), page);
        }
        return page;
    }

    @Override
    public Object visitControllerRule(ParserDSL.ControllerRuleContext ctx) {
        ArrayList<Function> functions = new ArrayList<>();
        for (int i = 0; i < ctx.function().size(); i++) {
            Function function;
            function = (Function) visitFunction(ctx.function().get(i));

            symbolTable.getSymbolTableController().put(function.getNameFunction(), function);
            symbolTable.getSymbolTableController().put("Variable", symbolTable.getSt_variable());
        }
        return functions;
    }

    public ArrayList<BlockStatement> visitBlockStatement(List<ParserDSL.BlockStatmentContext> ctxBlockStatement) {
        ArrayList<BlockStatement> blockStatements = new ArrayList<>();
        for (ParserDSL.BlockStatmentContext blockStatmentContext : ctxBlockStatement) {
            blockStatements.add((BlockStatement) visitBlockStatment(blockStatmentContext));
        }
        return blockStatements;
    }

    @Override
    public Object visitVariableDefine(ParserDSL.VariableDefineContext ctx) {


        Variable variable = new Variable();

        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < ctx.Text().size(); i++) {
            stringBuilder.append(ctx.Text().get(i));
        }
        variable.setScope(new Scope(ctx.start.getLine(), ctx.stop.getLine()));
        variable.setNameVariable(String.valueOf(stringBuilder));
        StringBuilder valueVariable = new StringBuilder();
        if (!ctx.readFromFileData().isEmpty()) {

            boolean leftSideIsCorrect = SemanticCheck.checkIfRedefineVariable(symbolTable.getSt_variable(), variable);
            if (!leftSideIsCorrect) {
                variable.setValue(visitReadFromFileData(ctx.readFromFileData().get(0)));
                symbolTable.getSt_variable().put(variable.getNameVariable(), variable);
            } else {
                writeSemanticError("Warning : you Try to redefine name variable", variable);
                StoreError.counter++;
            }

        } else {

            if (!ctx.TextNameVariable().isEmpty()) {
                for (int i = 0; i < ctx.TextNameVariable().size(); i++) {
                    valueVariable.append(ctx.TextNameVariable().get(i));
                }
            }
            boolean isString = (!ctx.DoubleQuateSecond().isEmpty() || !ctx.SingleQuateSecond().isEmpty());
            if (isString) {
                variable.setValue(String.valueOf(valueVariable));
            } else {
                if (!ctx.expression().isEmpty()) {
                    variable.setValue(visitExpression(ctx.expression().get(0)));
                } else {
                    char[] d = String.valueOf(valueVariable).toCharArray();
                    if (d[0] >= '0' && d[0] <= '9') {
                        variable.setValue(String.valueOf(valueVariable));
                    } else {
                        boolean isThereVar = symbolTable.getSt_variable().containsKey(String.valueOf(valueVariable));
                        if (!isThereVar)
                            variable.setValue(new Variable(new Scope(ctx.start.getLine(), ctx.stop.getLine()), String.valueOf(valueVariable), new Object()));
                        else {
                            variable.setValue(symbolTable.getSt_variable().get(String.valueOf(valueVariable)));
                        }
                    }
                }
            }
            boolean leftSideIsCorrect = SemanticCheck.checkIfRedefineVariable(symbolTable.getSt_variable(), variable);
            if (variable.getValue() instanceof Variable) {
                boolean rightSideIsCorrect =
                        SemanticCheck.checkIfVariableInConditionIsDefine(symbolTable.getSt_variable(), ((Variable) variable.getValue()));

                if (!leftSideIsCorrect && rightSideIsCorrect) {
                    if (((Variable) variable.getValue()).isParameter()) {
                        String ss = "_POST['" + ((Variable) variable.getValue()).getNameVariable() + "']";
                        variable.setValue(new Variable(variable.getScope(), ss, new Object(), true));
                    }
                    symbolTable.getSt_variable().put(variable.getNameVariable(), variable);
                } else {
                    if (leftSideIsCorrect) {
                        writeSemanticError("Warning : you Try to redefine name variable", variable);
                        StoreError.counter++;
                    }
                    if (!rightSideIsCorrect) {
//                    Variable variableTemp = (Variable) symbolTable.getSt_variable().get(((Variable) variable.getValue()).getNameVariable());
//                    if (variableTemp.isParameter()) {
//
//                        String s = "_POST['" + ((Variable) variable.getValue()).getNameVariable() + "']";
//                        variable.setValue(new Variable(variable.getScope(), s, new Object(), true));
//                    } else {

                        writeSemanticError("Warning : you Try to use variable not declare", (Variable) variable.getValue());
                        StoreError.counter++;

                    }
                }
            } else {

                if (variable.getValue() instanceof Expression) {
                } else {
                    symbolTable.getSt_variable().put(variable.getNameVariable(), variable);

                }
            }
        }

        return variable;
    }

    @Override
    public Object visitReadFromFileData(ParserDSL.ReadFromFileDataContext ctx) {
        StringBuilder nameFile = new StringBuilder();
        for (int i = 0; i < ctx.Namereadfile().size(); i++) {
            nameFile.append(ctx.Namereadfile().get(i));
        }
        return new ReadFromFile(new Scope(ctx.start.getLine(), ctx.stop.getLine()), String.valueOf(nameFile));
    }

    @Override
    public Object visitIfCondition(ParserDSL.IfConditionContext ctx) {
        IfStatement ifStatement = new IfStatement();
        StoreError.rangeIsAvailableDefine.add(new StoreError.Pair(ctx.start.getLine() + 1, ctx.stop.getLine()));
        if (!ctx.And().isEmpty())
            ifStatement.getLogicalOperators().add(ctx.And().get(0).getText());
        if (!ctx.Or().isEmpty())
            ifStatement.getLogicalOperators().add(ctx.Or().get(0).getText());

        for (int i = 0; i < ctx.conditionStatment().size(); i++) {
            ifStatement.getCondition().add((Condition) visitConditionStatment(ctx.conditionStatment().get(i)));
        }

        ifStatement.setScope(new Scope(ctx.start.getLine(), ctx.stop.getLine()));
        ifStatement.setBlockStatement(visitBlockStatement(ctx.blockStatment()));
        symbolTable.getSymbolTableController().put("If", ifStatement);
        return ifStatement;
    }

    @Override
    public Object visitConditionStatment(ParserDSL.ConditionStatmentContext ctx) {
        Expression expression = new Expression();

        expression.setFirst(visitFirstParty(ctx.firstParty()));
        expression.setSecond(visitSecondParty(ctx.secondParty()));
        if (expression.getFirst() instanceof Variable) {
            boolean check =
                    SemanticCheck.checkIfVariableInConditionIsDefine(symbolTable.getSt_variable(), ((Variable) expression.getFirst()));
            if (!check) {
                StoreError.haveSemanticError = true;
                counter++;
                writeSemanticError("Warning : you Try to use variable not declare", (Variable) expression.getFirst());
            } else {
                Variable x = (Variable) expression.getFirst();
                Variable temp = (Variable) symbolTable.getSt_variable().get(x.getNameVariable());
                if (temp.isParameter()) {
                    String ss = "_POST['" + temp.getNameVariable() + "']";
                    expression.setFirst(new Variable(temp.getScope(), ss, new Object(), true));
                }
            }
        }
        if (expression.getSecond() instanceof Variable) {
            boolean check =
                    SemanticCheck.checkIfVariableInConditionIsDefine(symbolTable.getSt_variable(), ((Variable) expression.getSecond()));
            if (!check) {
                StoreError.haveSemanticError = true;
                counter++;
                writeSemanticError("Warning : you Try to use variable not declare", (Variable) expression.getSecond());
            } else {
                Variable x = (Variable) expression.getSecond();
                Variable temp = (Variable) symbolTable.getSt_variable().get(x.getNameVariable());
                if (temp.isParameter()) {

                    String ss = "_POST['" + temp.getNameVariable() + "']";
                    expression.setSecond(new Variable(temp.getScope(), ss, new Object(), true));
                }
            }
        }
        if (ctx.EqualController() != null)
            expression.setOperator("==");
        if (ctx.EqualValueAndType() != null)
            expression.setOperator("===");
        Condition condition = new Condition();
        condition.setExpression(expression);
        return condition;
    }

    @Override
    public Object visit(ParseTree tree) {
        return super.visit(tree);
    }

    @Override
    public Object visitChildren(RuleNode node) {
        return super.visitChildren(node);
    }

    @Override
    public Object visitTerminal(TerminalNode node) {
        return super.visitTerminal(node);
    }

    @Override
    public Object visitErrorNode(ErrorNode node) {
        return super.visitErrorNode(node);
    }

    @Override
    protected Object defaultResult() {
        return super.defaultResult();
    }

    @Override
    public Object visitFirstParty(ParserDSL.FirstPartyContext ctx) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < ctx.Text().size(); i++) {
            stringBuilder.append(ctx.Text().get(i));
        }
        String s = String.valueOf(stringBuilder);
        char[] d = s.toCharArray();

        if (d[0] >= '0' && d[0] <= '9' || (!ctx.DoubleQuateController().isEmpty() || !ctx.SingleQuateController().isEmpty())) {
            return String.valueOf(stringBuilder);
        } else {
            return new Variable(new Scope(ctx.start.getLine(), ctx.stop.getLine()), s, new Object());
        }
    }

    @Override
    public Object visitSecondParty(ParserDSL.SecondPartyContext ctx) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int i = 0; i < ctx.Text().size(); i++) {
            stringBuilder.append(ctx.Text().get(i));
        }
        String s = String.valueOf(stringBuilder);
        char[] d = s.toCharArray();

        if (d[0] >= '0' && d[0] <= '9' || (!ctx.DoubleQuateController().isEmpty() || !ctx.SingleQuateController().isEmpty())) {
            return String.valueOf(stringBuilder);
        } else {
            return new Variable(new Scope(ctx.start.getLine(), ctx.stop.getLine()), s, new Object());
        }
    }

}