package ast.Node.controller;

import ast.Node.ParentNode;
import ast.Node.Scope;

public class Or extends ParentNode {

    String textOr;

    public Or() {
    }

    public Or(Scope scope, String textOr) {
        super.setScope(scope);
        this.textOr = textOr;
    }

    public String getTextOr() {
        return textOr;
    }

    public void setTextOr(String textOr) {
        this.textOr = textOr;
    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }


}
