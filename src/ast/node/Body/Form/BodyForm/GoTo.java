package ast.Node.Body.Form.BodyForm;

import ast.Node.ParentNode;
import ast.Node.Scope;

import java.util.Objects;

public class GoTo extends ParentNode {
    Scope scope;
    String namePageGoTo;

    public GoTo() {
    }

    public GoTo(Scope scope, String namePageGoTo) {
        super.setScope(scope);
        this.namePageGoTo = namePageGoTo;
    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }

    public String getNamePageGoTo() {
        return namePageGoTo;
    }

    public void setNamePageGoTo(String namePageGoTo) {
        this.namePageGoTo = namePageGoTo;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof GoTo goTo)) return false;
        return getScope().equals(goTo.getScope()) && getNamePageGoTo().equals(goTo.getNamePageGoTo());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getScope(), getNamePageGoTo());
    }

    @Override
    public String toString() {
        return "GoTo{" +
                "scope=" + scope +
                ", namePageGoTo='" + namePageGoTo + '\'' +
                '}';
    }
}
