package ast.Node.Body.Form.BodyForm;

import ast.Node.ParentNode;
import ast.Node.Scope;

public class Value extends ParentNode {
    String valueTextInValue;


    public Value() {
    }

    public Value(String valueTextInValue) {
        this.valueTextInValue = valueTextInValue;
    }

    public Value(String valueTextInValue, Scope scope) {
        this.valueTextInValue = valueTextInValue;
        super.setScope(scope);
    }

    public String getValueTextInValue() {
        return valueTextInValue;
    }

    public void setValueTextInValue(String valueTextInValue) {
        this.valueTextInValue = valueTextInValue;
    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }

}
