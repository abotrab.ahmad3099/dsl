// Generated from /Users/ahmadabotrab/Desktop/Compiler_2_Project/dsl/src/ParserDSL.g4 by ANTLR 4.10.1
import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link ParserDSL}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 * operations with no return type.
 */
public interface ParserDSLVisitor<T> extends ParseTreeVisitor<T> {
	/**
	 * Visit a parse tree produced by {@link ParserDSL#page}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitPage(ParserDSL.PageContext ctx);
	/**
	 * Visit a parse tree produced by {@link ParserDSL#title}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitTitle(ParserDSL.TitleContext ctx);
	/**
	 * Visit a parse tree produced by {@link ParserDSL#attributeInPage}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitAttributeInPage(ParserDSL.AttributeInPageContext ctx);
	/**
	 * Visit a parse tree produced by {@link ParserDSL#navbar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitNavbar(ParserDSL.NavbarContext ctx);
	/**
	 * Visit a parse tree produced by {@link ParserDSL#body}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBody(ParserDSL.BodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link ParserDSL#footer}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitFooter(ParserDSL.FooterContext ctx);
	/**
	 * Visit a parse tree produced by {@link ParserDSL#bodyNavBar}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBodyNavBar(ParserDSL.BodyNavBarContext ctx);
	/**
	 * Visit a parse tree produced by {@link ParserDSL#bodyBody}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBodyBody(ParserDSL.BodyBodyContext ctx);
	/**
	 * Visit a parse tree produced by {@link ParserDSL#bodyFooter}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitBodyFooter(ParserDSL.BodyFooterContext ctx);
	/**
	 * Visit a parse tree produced by {@link ParserDSL#htmlMisc}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHtmlMisc(ParserDSL.HtmlMiscContext ctx);
	/**
	 * Visit a parse tree produced by {@link ParserDSL#htmlComment}.
	 * @param ctx the parse tree
	 * @return the visitor result
	 */
	T visitHtmlComment(ParserDSL.HtmlCommentContext ctx);
}