package ast.Node.Body.Form.BodyForm;

import ast.Node.ParentNode;
import ast.Node.Scope;

public class RadioButton extends ParentNode {

    String nameRadioButton;

    public RadioButton() {
    }

    public RadioButton(Scope scope, String nameRadioButton) {
        super.setScope(scope);
        this.nameRadioButton = nameRadioButton;
    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }

    public String getNameRadioButton() {
        return nameRadioButton;
    }

    public void setNameRadioButton(String nameRadioButton) {
        this.nameRadioButton = nameRadioButton;
    }
}
