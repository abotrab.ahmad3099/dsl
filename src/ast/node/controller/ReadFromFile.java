package ast.Node.controller;

import ast.Node.ParentNode;
import ast.Node.Scope;

public class ReadFromFile extends ParentNode {

    Object nameFile;

    public ReadFromFile(Scope scope, Object nameFile) {
        super.setScope(scope);
        this.nameFile = nameFile;
    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }

    public Object getNameFile() {
        return nameFile;
    }

    public void setNameFile(Object nameFile) {
        this.nameFile = nameFile;
    }
}
