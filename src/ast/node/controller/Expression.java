package ast.Node.controller;

import java.util.Objects;

public class Expression {
    Object first;
    String operator;
    Object second;

    public Expression() {
    }

    public Expression(Object first, String operator, Object second) {
        this.first = first;
        this.operator = operator;
        this.second = second;
    }

    public Object getFirst() {
        return first;
    }

    public void setFirst(Object first) {
        this.first = first;
    }

    public String getOperator() {
        return operator;
    }

    public void setOperator(String operator) {
        this.operator = operator;
    }

    public Object getSecond() {
        return second;
    }

    public void setSecond(Object second) {
        this.second = second;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Expression that)) return false;
        return getFirst().equals(that.getFirst()) && getOperator().equals(that.getOperator()) && getSecond().equals(that.getSecond());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getFirst(), getOperator(), getSecond());
    }

    @Override
    public String toString() {
        return "Expression{" +
                "first=" + first +
                ", operator='" + operator + '\'' +
                ", second=" + second +
                '}';
    }
}
