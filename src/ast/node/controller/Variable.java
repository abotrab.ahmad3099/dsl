package ast.Node.controller;

import ast.Node.ParentNode;
import ast.Node.Scope;

public class Variable extends ParentNode {

    String nameVariable;
    Object value;

    boolean isParameter;

    public Variable() {
        isParameter = false;
    }

    public Variable(Scope scope, String nameVariable, Object value, boolean isParameter) {
        super.setScope(scope);
        this.nameVariable = nameVariable;
        this.value = value;
        this.isParameter = isParameter;
    }

    public Variable(Scope scope, String nameVariable, Object value) {
        super.setScope(scope);
        this.nameVariable = nameVariable;
        this.value = value;

    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }

    public String getNameVariable() {
        return nameVariable;
    }

    public void setNameVariable(String nameVariable) {
        this.nameVariable = nameVariable;
    }

    public Object getValue() {
        return value;
    }

    public void setValue(Object value) {
        this.value = value;
    }

    public boolean isParameter() {
        return isParameter;
    }

    public void setParameter(boolean parameter) {
        isParameter = parameter;
    }
}
