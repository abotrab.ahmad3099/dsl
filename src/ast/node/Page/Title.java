package ast.Node.Page;

import ast.Node.ParentNode;
import ast.Node.Scope;

public class Title extends ParentNode {
    String titleString;


    public Title(String titleString) {
        this.titleString = titleString;
    }

    public Title(String titleString, Scope scope) {
        this.titleString = titleString;
        super.setScope(scope);
    }

    public Title() {

    }

    public String getTitleString() {
        return titleString;
    }

    public void setTitleString(String titleString) {
        this.titleString = titleString;
    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }

}
