// Generated from /Users/ahmadabotrab/Desktop/Compiler_2_Project/dsl/src/ParserDSL.g4 by ANTLR 4.10.1
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class ParserDSL extends Parser {
	static { RuntimeMetaData.checkVersion("4.10.1", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		PAGE=1, NAMEPAGE=2, PAGEOPEN=3, AttributDefault=4, OperationDefault=5, 
		HTML_COMMENT=6, HTML_CONDITIONAL_COMMENT=7, TITLE=8, SingleQuatePage=9, 
		DoubleQuatePage=10, TextInPage=11, OperationInPage=12, DigitInPage=13, 
		AttributeInPage=14, OpenBracket_PAGE=15, CloseBracket_PAGE=16, SimiColonPage=17, 
		NAVBAR=18, BODY=19, FOOTER=20, NAVBAR_inMode=21, SingleQuateNav=22, DoubleQuateNav=23, 
		TextInNav=24, OperationInNav=25, DigitInNav=26, AttributeInNav=27, OpenBracket_NAV=28, 
		CloseBracket_NAV=29, SimiColonNav=30, BodyInMode=31, SingleQuateBody=32, 
		DoubleQuateBody=33, TextInBody=34, OperationInBody=35, DigitInBody=36, 
		AttributeInBody=37, OpenBracket_BODY=38, CloseBracket_BODY=39, SimiColonBody=40, 
		FOOTERNINMODE=41, SingleQuateFooter=42, DoubleQuateFooter=43, TextInFooter=44, 
		OpenBracket_FOOTER=45, CloseBracket_FOOTER=46, OperationInFooter=47, DigitInFooter=48, 
		AttributeInFooter=49, SIMICOLON_inModeFooter=50;
	public static final int
		RULE_page = 0, RULE_title = 1, RULE_attributeInPage = 2, RULE_navbar = 3, 
		RULE_body = 4, RULE_footer = 5, RULE_bodyNavBar = 6, RULE_bodyBody = 7, 
		RULE_bodyFooter = 8, RULE_htmlMisc = 9, RULE_htmlComment = 10;
	private static String[] makeRuleNames() {
		return new String[] {
			"page", "title", "attributeInPage", "navbar", "body", "footer", "bodyNavBar", 
			"bodyBody", "bodyFooter", "htmlMisc", "htmlComment"
		};
	}
	public static final String[] ruleNames = makeRuleNames();

	private static String[] makeLiteralNames() {
		return new String[] {
			null, "'page'", null, null, null, null, null, "'//'", "'title'", null, 
			null, null, null, null, null, null, null, null, "'navbar'", "'body'", 
			"'footer'"
		};
	}
	private static final String[] _LITERAL_NAMES = makeLiteralNames();
	private static String[] makeSymbolicNames() {
		return new String[] {
			null, "PAGE", "NAMEPAGE", "PAGEOPEN", "AttributDefault", "OperationDefault", 
			"HTML_COMMENT", "HTML_CONDITIONAL_COMMENT", "TITLE", "SingleQuatePage", 
			"DoubleQuatePage", "TextInPage", "OperationInPage", "DigitInPage", "AttributeInPage", 
			"OpenBracket_PAGE", "CloseBracket_PAGE", "SimiColonPage", "NAVBAR", "BODY", 
			"FOOTER", "NAVBAR_inMode", "SingleQuateNav", "DoubleQuateNav", "TextInNav", 
			"OperationInNav", "DigitInNav", "AttributeInNav", "OpenBracket_NAV", 
			"CloseBracket_NAV", "SimiColonNav", "BodyInMode", "SingleQuateBody", 
			"DoubleQuateBody", "TextInBody", "OperationInBody", "DigitInBody", "AttributeInBody", 
			"OpenBracket_BODY", "CloseBracket_BODY", "SimiColonBody", "FOOTERNINMODE", 
			"SingleQuateFooter", "DoubleQuateFooter", "TextInFooter", "OpenBracket_FOOTER", 
			"CloseBracket_FOOTER", "OperationInFooter", "DigitInFooter", "AttributeInFooter", 
			"SIMICOLON_inModeFooter"
		};
	}
	private static final String[] _SYMBOLIC_NAMES = makeSymbolicNames();
	public static final Vocabulary VOCABULARY = new VocabularyImpl(_LITERAL_NAMES, _SYMBOLIC_NAMES);

	/**
	 * @deprecated Use {@link #VOCABULARY} instead.
	 */
	@Deprecated
	public static final String[] tokenNames;
	static {
		tokenNames = new String[_SYMBOLIC_NAMES.length];
		for (int i = 0; i < tokenNames.length; i++) {
			tokenNames[i] = VOCABULARY.getLiteralName(i);
			if (tokenNames[i] == null) {
				tokenNames[i] = VOCABULARY.getSymbolicName(i);
			}

			if (tokenNames[i] == null) {
				tokenNames[i] = "<INVALID>";
			}
		}
	}

	@Override
	@Deprecated
	public String[] getTokenNames() {
		return tokenNames;
	}

	@Override

	public Vocabulary getVocabulary() {
		return VOCABULARY;
	}

	@Override
	public String getGrammarFileName() { return "ParserDSL.g4"; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public ParserDSL(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	public static class PageContext extends ParserRuleContext {
		public TerminalNode PAGE() { return getToken(ParserDSL.PAGE, 0); }
		public TerminalNode OperationDefault() { return getToken(ParserDSL.OperationDefault, 0); }
		public TerminalNode PAGEOPEN() { return getToken(ParserDSL.PAGEOPEN, 0); }
		public TerminalNode OpenBracket_PAGE() { return getToken(ParserDSL.OpenBracket_PAGE, 0); }
		public AttributeInPageContext attributeInPage() {
			return getRuleContext(AttributeInPageContext.class,0);
		}
		public TerminalNode CloseBracket_PAGE() { return getToken(ParserDSL.CloseBracket_PAGE, 0); }
		public TerminalNode SimiColonPage() { return getToken(ParserDSL.SimiColonPage, 0); }
		public List<TerminalNode> AttributDefault() { return getTokens(ParserDSL.AttributDefault); }
		public TerminalNode AttributDefault(int i) {
			return getToken(ParserDSL.AttributDefault, i);
		}
		public List<TerminalNode> AttributeInPage() { return getTokens(ParserDSL.AttributeInPage); }
		public TerminalNode AttributeInPage(int i) {
			return getToken(ParserDSL.AttributeInPage, i);
		}
		public List<TitleContext> title() {
			return getRuleContexts(TitleContext.class);
		}
		public TitleContext title(int i) {
			return getRuleContext(TitleContext.class,i);
		}
		public PageContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_page; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParserDSLListener ) ((ParserDSLListener)listener).enterPage(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParserDSLListener ) ((ParserDSLListener)listener).exitPage(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserDSLVisitor ) return ((ParserDSLVisitor<? extends T>)visitor).visitPage(this);
			else return visitor.visitChildren(this);
		}
	}

	public final PageContext page() throws RecognitionException {
		PageContext _localctx = new PageContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_page);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(25);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AttributDefault) {
				{
				{
				setState(22);
				match(AttributDefault);
				}
				}
				setState(27);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(28);
			match(PAGE);
			setState(32);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AttributDefault) {
				{
				{
				setState(29);
				match(AttributDefault);
				}
				}
				setState(34);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(35);
			match(OperationDefault);
			setState(39);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AttributDefault) {
				{
				{
				setState(36);
				match(AttributDefault);
				}
				}
				setState(41);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(42);
			match(PAGEOPEN);
			setState(46);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AttributeInPage) {
				{
				{
				setState(43);
				match(AttributeInPage);
				}
				}
				setState(48);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(49);
			match(OpenBracket_PAGE);
			setState(53);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(50);
					match(AttributeInPage);
					}
					} 
				}
				setState(55);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,4,_ctx);
			}
			setState(59);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==TITLE) {
				{
				{
				setState(56);
				title();
				}
				}
				setState(61);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(62);
			attributeInPage();
			setState(63);
			match(CloseBracket_PAGE);
			setState(67);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AttributDefault) {
				{
				{
				setState(64);
				match(AttributDefault);
				}
				}
				setState(69);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(70);
			match(SimiColonPage);
			setState(74);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AttributDefault) {
				{
				{
				setState(71);
				match(AttributDefault);
				}
				}
				setState(76);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TitleContext extends ParserRuleContext {
		public TerminalNode TITLE() { return getToken(ParserDSL.TITLE, 0); }
		public List<TerminalNode> OperationInPage() { return getTokens(ParserDSL.OperationInPage); }
		public TerminalNode OperationInPage(int i) {
			return getToken(ParserDSL.OperationInPage, i);
		}
		public List<TerminalNode> SingleQuatePage() { return getTokens(ParserDSL.SingleQuatePage); }
		public TerminalNode SingleQuatePage(int i) {
			return getToken(ParserDSL.SingleQuatePage, i);
		}
		public List<TerminalNode> AttributeInPage() { return getTokens(ParserDSL.AttributeInPage); }
		public TerminalNode AttributeInPage(int i) {
			return getToken(ParserDSL.AttributeInPage, i);
		}
		public List<TerminalNode> TextInPage() { return getTokens(ParserDSL.TextInPage); }
		public TerminalNode TextInPage(int i) {
			return getToken(ParserDSL.TextInPage, i);
		}
		public TitleContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_title; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParserDSLListener ) ((ParserDSLListener)listener).enterTitle(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParserDSLListener ) ((ParserDSLListener)listener).exitTitle(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserDSLVisitor ) return ((ParserDSLVisitor<? extends T>)visitor).visitTitle(this);
			else return visitor.visitChildren(this);
		}
	}

	public final TitleContext title() throws RecognitionException {
		TitleContext _localctx = new TitleContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_title);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(77);
			match(TITLE);
			setState(81);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AttributeInPage) {
				{
				{
				setState(78);
				match(AttributeInPage);
				}
				}
				setState(83);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(84);
			match(OperationInPage);
			setState(88);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AttributeInPage) {
				{
				{
				setState(85);
				match(AttributeInPage);
				}
				}
				setState(90);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(91);
			match(SingleQuatePage);
			setState(95);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(92);
					match(AttributeInPage);
					}
					} 
				}
				setState(97);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,10,_ctx);
			}
			setState(101);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==TextInPage) {
				{
				{
				setState(98);
				match(TextInPage);
				}
				}
				setState(103);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(107);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AttributeInPage) {
				{
				{
				setState(104);
				match(AttributeInPage);
				}
				}
				setState(109);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(110);
			match(SingleQuatePage);
			setState(114);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AttributeInPage) {
				{
				{
				setState(111);
				match(AttributeInPage);
				}
				}
				setState(116);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(117);
			match(OperationInPage);
			setState(121);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,14,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(118);
					match(AttributeInPage);
					}
					} 
				}
				setState(123);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,14,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class AttributeInPageContext extends ParserRuleContext {
		public List<NavbarContext> navbar() {
			return getRuleContexts(NavbarContext.class);
		}
		public NavbarContext navbar(int i) {
			return getRuleContext(NavbarContext.class,i);
		}
		public List<TerminalNode> AttributeInPage() { return getTokens(ParserDSL.AttributeInPage); }
		public TerminalNode AttributeInPage(int i) {
			return getToken(ParserDSL.AttributeInPage, i);
		}
		public List<BodyContext> body() {
			return getRuleContexts(BodyContext.class);
		}
		public BodyContext body(int i) {
			return getRuleContext(BodyContext.class,i);
		}
		public List<FooterContext> footer() {
			return getRuleContexts(FooterContext.class);
		}
		public FooterContext footer(int i) {
			return getRuleContext(FooterContext.class,i);
		}
		public AttributeInPageContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_attributeInPage; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParserDSLListener ) ((ParserDSLListener)listener).enterAttributeInPage(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParserDSLListener ) ((ParserDSLListener)listener).exitAttributeInPage(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserDSLVisitor ) return ((ParserDSLVisitor<? extends T>)visitor).visitAttributeInPage(this);
			else return visitor.visitChildren(this);
		}
	}

	public final AttributeInPageContext attributeInPage() throws RecognitionException {
		AttributeInPageContext _localctx = new AttributeInPageContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_attributeInPage);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(127);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==NAVBAR) {
				{
				{
				setState(124);
				navbar();
				}
				}
				setState(129);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(133);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(130);
					match(AttributeInPage);
					}
					} 
				}
				setState(135);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,16,_ctx);
			}
			setState(139);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==BODY) {
				{
				{
				setState(136);
				body();
				}
				}
				setState(141);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(145);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(142);
					match(AttributeInPage);
					}
					} 
				}
				setState(147);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,18,_ctx);
			}
			setState(151);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==FOOTER) {
				{
				{
				setState(148);
				footer();
				}
				}
				setState(153);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(157);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AttributeInPage) {
				{
				{
				setState(154);
				match(AttributeInPage);
				}
				}
				setState(159);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class NavbarContext extends ParserRuleContext {
		public TerminalNode NAVBAR() { return getToken(ParserDSL.NAVBAR, 0); }
		public TerminalNode OperationInNav() { return getToken(ParserDSL.OperationInNav, 0); }
		public TerminalNode NAVBAR_inMode() { return getToken(ParserDSL.NAVBAR_inMode, 0); }
		public TerminalNode OpenBracket_NAV() { return getToken(ParserDSL.OpenBracket_NAV, 0); }
		public TerminalNode CloseBracket_NAV() { return getToken(ParserDSL.CloseBracket_NAV, 0); }
		public TerminalNode SimiColonNav() { return getToken(ParserDSL.SimiColonNav, 0); }
		public List<TerminalNode> AttributeInNav() { return getTokens(ParserDSL.AttributeInNav); }
		public TerminalNode AttributeInNav(int i) {
			return getToken(ParserDSL.AttributeInNav, i);
		}
		public List<BodyNavBarContext> bodyNavBar() {
			return getRuleContexts(BodyNavBarContext.class);
		}
		public BodyNavBarContext bodyNavBar(int i) {
			return getRuleContext(BodyNavBarContext.class,i);
		}
		public NavbarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_navbar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParserDSLListener ) ((ParserDSLListener)listener).enterNavbar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParserDSLListener ) ((ParserDSLListener)listener).exitNavbar(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserDSLVisitor ) return ((ParserDSLVisitor<? extends T>)visitor).visitNavbar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final NavbarContext navbar() throws RecognitionException {
		NavbarContext _localctx = new NavbarContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_navbar);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(160);
			match(NAVBAR);
			setState(164);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AttributeInNav) {
				{
				{
				setState(161);
				match(AttributeInNav);
				}
				}
				setState(166);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(167);
			match(OperationInNav);
			setState(171);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AttributeInNav) {
				{
				{
				setState(168);
				match(AttributeInNav);
				}
				}
				setState(173);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(174);
			match(NAVBAR_inMode);
			setState(178);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AttributeInNav) {
				{
				{
				setState(175);
				match(AttributeInNav);
				}
				}
				setState(180);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(181);
			match(OpenBracket_NAV);
			setState(185);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(182);
					match(AttributeInNav);
					}
					} 
				}
				setState(187);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,24,_ctx);
			}
			setState(191);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==TextInNav) {
				{
				{
				setState(188);
				bodyNavBar();
				}
				}
				setState(193);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(197);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AttributeInNav) {
				{
				{
				setState(194);
				match(AttributeInNav);
				}
				}
				setState(199);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(200);
			match(CloseBracket_NAV);
			setState(204);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AttributeInNav) {
				{
				{
				setState(201);
				match(AttributeInNav);
				}
				}
				setState(206);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(207);
			match(SimiColonNav);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BodyContext extends ParserRuleContext {
		public TerminalNode BODY() { return getToken(ParserDSL.BODY, 0); }
		public TerminalNode OperationInBody() { return getToken(ParserDSL.OperationInBody, 0); }
		public TerminalNode BodyInMode() { return getToken(ParserDSL.BodyInMode, 0); }
		public TerminalNode OpenBracket_BODY() { return getToken(ParserDSL.OpenBracket_BODY, 0); }
		public TerminalNode CloseBracket_BODY() { return getToken(ParserDSL.CloseBracket_BODY, 0); }
		public TerminalNode SimiColonBody() { return getToken(ParserDSL.SimiColonBody, 0); }
		public List<TerminalNode> AttributeInBody() { return getTokens(ParserDSL.AttributeInBody); }
		public TerminalNode AttributeInBody(int i) {
			return getToken(ParserDSL.AttributeInBody, i);
		}
		public List<BodyBodyContext> bodyBody() {
			return getRuleContexts(BodyBodyContext.class);
		}
		public BodyBodyContext bodyBody(int i) {
			return getRuleContext(BodyBodyContext.class,i);
		}
		public BodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_body; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParserDSLListener ) ((ParserDSLListener)listener).enterBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParserDSLListener ) ((ParserDSLListener)listener).exitBody(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserDSLVisitor ) return ((ParserDSLVisitor<? extends T>)visitor).visitBody(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BodyContext body() throws RecognitionException {
		BodyContext _localctx = new BodyContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_body);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(209);
			match(BODY);
			setState(213);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AttributeInBody) {
				{
				{
				setState(210);
				match(AttributeInBody);
				}
				}
				setState(215);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(216);
			match(OperationInBody);
			setState(220);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AttributeInBody) {
				{
				{
				setState(217);
				match(AttributeInBody);
				}
				}
				setState(222);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(223);
			match(BodyInMode);
			setState(227);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AttributeInBody) {
				{
				{
				setState(224);
				match(AttributeInBody);
				}
				}
				setState(229);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(230);
			match(OpenBracket_BODY);
			setState(234);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,31,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(231);
					match(AttributeInBody);
					}
					} 
				}
				setState(236);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,31,_ctx);
			}
			setState(240);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==TextInBody) {
				{
				{
				setState(237);
				bodyBody();
				}
				}
				setState(242);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(246);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AttributeInBody) {
				{
				{
				setState(243);
				match(AttributeInBody);
				}
				}
				setState(248);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(249);
			match(CloseBracket_BODY);
			setState(253);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AttributeInBody) {
				{
				{
				setState(250);
				match(AttributeInBody);
				}
				}
				setState(255);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(256);
			match(SimiColonBody);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class FooterContext extends ParserRuleContext {
		public TerminalNode FOOTER() { return getToken(ParserDSL.FOOTER, 0); }
		public TerminalNode OperationInFooter() { return getToken(ParserDSL.OperationInFooter, 0); }
		public TerminalNode FOOTERNINMODE() { return getToken(ParserDSL.FOOTERNINMODE, 0); }
		public TerminalNode OpenBracket_FOOTER() { return getToken(ParserDSL.OpenBracket_FOOTER, 0); }
		public TerminalNode CloseBracket_FOOTER() { return getToken(ParserDSL.CloseBracket_FOOTER, 0); }
		public TerminalNode SIMICOLON_inModeFooter() { return getToken(ParserDSL.SIMICOLON_inModeFooter, 0); }
		public List<TerminalNode> AttributeInFooter() { return getTokens(ParserDSL.AttributeInFooter); }
		public TerminalNode AttributeInFooter(int i) {
			return getToken(ParserDSL.AttributeInFooter, i);
		}
		public List<BodyFooterContext> bodyFooter() {
			return getRuleContexts(BodyFooterContext.class);
		}
		public BodyFooterContext bodyFooter(int i) {
			return getRuleContext(BodyFooterContext.class,i);
		}
		public FooterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_footer; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParserDSLListener ) ((ParserDSLListener)listener).enterFooter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParserDSLListener ) ((ParserDSLListener)listener).exitFooter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserDSLVisitor ) return ((ParserDSLVisitor<? extends T>)visitor).visitFooter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final FooterContext footer() throws RecognitionException {
		FooterContext _localctx = new FooterContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_footer);
		int _la;
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(258);
			match(FOOTER);
			setState(262);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AttributeInFooter) {
				{
				{
				setState(259);
				match(AttributeInFooter);
				}
				}
				setState(264);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(265);
			match(OperationInFooter);
			setState(269);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AttributeInFooter) {
				{
				{
				setState(266);
				match(AttributeInFooter);
				}
				}
				setState(271);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(272);
			match(FOOTERNINMODE);
			setState(276);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AttributeInFooter) {
				{
				{
				setState(273);
				match(AttributeInFooter);
				}
				}
				setState(278);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(279);
			match(OpenBracket_FOOTER);
			setState(283);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,38,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(280);
					match(AttributeInFooter);
					}
					} 
				}
				setState(285);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,38,_ctx);
			}
			setState(289);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==TextInFooter) {
				{
				{
				setState(286);
				bodyFooter();
				}
				}
				setState(291);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(295);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AttributeInFooter) {
				{
				{
				setState(292);
				match(AttributeInFooter);
				}
				}
				setState(297);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(298);
			match(CloseBracket_FOOTER);
			setState(302);
			_errHandler.sync(this);
			_la = _input.LA(1);
			while (_la==AttributeInFooter) {
				{
				{
				setState(299);
				match(AttributeInFooter);
				}
				}
				setState(304);
				_errHandler.sync(this);
				_la = _input.LA(1);
			}
			setState(305);
			match(SIMICOLON_inModeFooter);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BodyNavBarContext extends ParserRuleContext {
		public TerminalNode TextInNav() { return getToken(ParserDSL.TextInNav, 0); }
		public BodyNavBarContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bodyNavBar; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParserDSLListener ) ((ParserDSLListener)listener).enterBodyNavBar(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParserDSLListener ) ((ParserDSLListener)listener).exitBodyNavBar(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserDSLVisitor ) return ((ParserDSLVisitor<? extends T>)visitor).visitBodyNavBar(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BodyNavBarContext bodyNavBar() throws RecognitionException {
		BodyNavBarContext _localctx = new BodyNavBarContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_bodyNavBar);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(307);
			match(TextInNav);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BodyBodyContext extends ParserRuleContext {
		public TerminalNode TextInBody() { return getToken(ParserDSL.TextInBody, 0); }
		public BodyBodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bodyBody; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParserDSLListener ) ((ParserDSLListener)listener).enterBodyBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParserDSLListener ) ((ParserDSLListener)listener).exitBodyBody(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserDSLVisitor ) return ((ParserDSLVisitor<? extends T>)visitor).visitBodyBody(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BodyBodyContext bodyBody() throws RecognitionException {
		BodyBodyContext _localctx = new BodyBodyContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_bodyBody);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(309);
			match(TextInBody);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BodyFooterContext extends ParserRuleContext {
		public TerminalNode TextInFooter() { return getToken(ParserDSL.TextInFooter, 0); }
		public BodyFooterContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_bodyFooter; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParserDSLListener ) ((ParserDSLListener)listener).enterBodyFooter(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParserDSLListener ) ((ParserDSLListener)listener).exitBodyFooter(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserDSLVisitor ) return ((ParserDSLVisitor<? extends T>)visitor).visitBodyFooter(this);
			else return visitor.visitChildren(this);
		}
	}

	public final BodyFooterContext bodyFooter() throws RecognitionException {
		BodyFooterContext _localctx = new BodyFooterContext(_ctx, getState());
		enterRule(_localctx, 16, RULE_bodyFooter);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(311);
			match(TextInFooter);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HtmlMiscContext extends ParserRuleContext {
		public HtmlCommentContext htmlComment() {
			return getRuleContext(HtmlCommentContext.class,0);
		}
		public TerminalNode AttributDefault() { return getToken(ParserDSL.AttributDefault, 0); }
		public HtmlMiscContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_htmlMisc; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParserDSLListener ) ((ParserDSLListener)listener).enterHtmlMisc(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParserDSLListener ) ((ParserDSLListener)listener).exitHtmlMisc(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserDSLVisitor ) return ((ParserDSLVisitor<? extends T>)visitor).visitHtmlMisc(this);
			else return visitor.visitChildren(this);
		}
	}

	public final HtmlMiscContext htmlMisc() throws RecognitionException {
		HtmlMiscContext _localctx = new HtmlMiscContext(_ctx, getState());
		enterRule(_localctx, 18, RULE_htmlMisc);
		try {
			setState(315);
			_errHandler.sync(this);
			switch (_input.LA(1)) {
			case HTML_COMMENT:
			case HTML_CONDITIONAL_COMMENT:
				enterOuterAlt(_localctx, 1);
				{
				setState(313);
				htmlComment();
				}
				break;
			case AttributDefault:
				enterOuterAlt(_localctx, 2);
				{
				setState(314);
				match(AttributDefault);
				}
				break;
			default:
				throw new NoViableAltException(this);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HtmlCommentContext extends ParserRuleContext {
		public TerminalNode HTML_COMMENT() { return getToken(ParserDSL.HTML_COMMENT, 0); }
		public TerminalNode HTML_CONDITIONAL_COMMENT() { return getToken(ParserDSL.HTML_CONDITIONAL_COMMENT, 0); }
		public HtmlCommentContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_htmlComment; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof ParserDSLListener ) ((ParserDSLListener)listener).enterHtmlComment(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof ParserDSLListener ) ((ParserDSLListener)listener).exitHtmlComment(this);
		}
		@Override
		public <T> T accept(ParseTreeVisitor<? extends T> visitor) {
			if ( visitor instanceof ParserDSLVisitor ) return ((ParserDSLVisitor<? extends T>)visitor).visitHtmlComment(this);
			else return visitor.visitChildren(this);
		}
	}

	public final HtmlCommentContext htmlComment() throws RecognitionException {
		HtmlCommentContext _localctx = new HtmlCommentContext(_ctx, getState());
		enterRule(_localctx, 20, RULE_htmlComment);
		int _la;
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(317);
			_la = _input.LA(1);
			if ( !(_la==HTML_COMMENT || _la==HTML_CONDITIONAL_COMMENT) ) {
			_errHandler.recoverInline(this);
			}
			else {
				if ( _input.LA(1)==Token.EOF ) matchedEOF = true;
				_errHandler.reportMatch(this);
				consume();
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\u0004\u00012\u0140\u0002\u0000\u0007\u0000\u0002\u0001\u0007\u0001\u0002"+
		"\u0002\u0007\u0002\u0002\u0003\u0007\u0003\u0002\u0004\u0007\u0004\u0002"+
		"\u0005\u0007\u0005\u0002\u0006\u0007\u0006\u0002\u0007\u0007\u0007\u0002"+
		"\b\u0007\b\u0002\t\u0007\t\u0002\n\u0007\n\u0001\u0000\u0005\u0000\u0018"+
		"\b\u0000\n\u0000\f\u0000\u001b\t\u0000\u0001\u0000\u0001\u0000\u0005\u0000"+
		"\u001f\b\u0000\n\u0000\f\u0000\"\t\u0000\u0001\u0000\u0001\u0000\u0005"+
		"\u0000&\b\u0000\n\u0000\f\u0000)\t\u0000\u0001\u0000\u0001\u0000\u0005"+
		"\u0000-\b\u0000\n\u0000\f\u00000\t\u0000\u0001\u0000\u0001\u0000\u0005"+
		"\u00004\b\u0000\n\u0000\f\u00007\t\u0000\u0001\u0000\u0005\u0000:\b\u0000"+
		"\n\u0000\f\u0000=\t\u0000\u0001\u0000\u0001\u0000\u0001\u0000\u0005\u0000"+
		"B\b\u0000\n\u0000\f\u0000E\t\u0000\u0001\u0000\u0001\u0000\u0005\u0000"+
		"I\b\u0000\n\u0000\f\u0000L\t\u0000\u0001\u0001\u0001\u0001\u0005\u0001"+
		"P\b\u0001\n\u0001\f\u0001S\t\u0001\u0001\u0001\u0001\u0001\u0005\u0001"+
		"W\b\u0001\n\u0001\f\u0001Z\t\u0001\u0001\u0001\u0001\u0001\u0005\u0001"+
		"^\b\u0001\n\u0001\f\u0001a\t\u0001\u0001\u0001\u0005\u0001d\b\u0001\n"+
		"\u0001\f\u0001g\t\u0001\u0001\u0001\u0005\u0001j\b\u0001\n\u0001\f\u0001"+
		"m\t\u0001\u0001\u0001\u0001\u0001\u0005\u0001q\b\u0001\n\u0001\f\u0001"+
		"t\t\u0001\u0001\u0001\u0001\u0001\u0005\u0001x\b\u0001\n\u0001\f\u0001"+
		"{\t\u0001\u0001\u0002\u0005\u0002~\b\u0002\n\u0002\f\u0002\u0081\t\u0002"+
		"\u0001\u0002\u0005\u0002\u0084\b\u0002\n\u0002\f\u0002\u0087\t\u0002\u0001"+
		"\u0002\u0005\u0002\u008a\b\u0002\n\u0002\f\u0002\u008d\t\u0002\u0001\u0002"+
		"\u0005\u0002\u0090\b\u0002\n\u0002\f\u0002\u0093\t\u0002\u0001\u0002\u0005"+
		"\u0002\u0096\b\u0002\n\u0002\f\u0002\u0099\t\u0002\u0001\u0002\u0005\u0002"+
		"\u009c\b\u0002\n\u0002\f\u0002\u009f\t\u0002\u0001\u0003\u0001\u0003\u0005"+
		"\u0003\u00a3\b\u0003\n\u0003\f\u0003\u00a6\t\u0003\u0001\u0003\u0001\u0003"+
		"\u0005\u0003\u00aa\b\u0003\n\u0003\f\u0003\u00ad\t\u0003\u0001\u0003\u0001"+
		"\u0003\u0005\u0003\u00b1\b\u0003\n\u0003\f\u0003\u00b4\t\u0003\u0001\u0003"+
		"\u0001\u0003\u0005\u0003\u00b8\b\u0003\n\u0003\f\u0003\u00bb\t\u0003\u0001"+
		"\u0003\u0005\u0003\u00be\b\u0003\n\u0003\f\u0003\u00c1\t\u0003\u0001\u0003"+
		"\u0005\u0003\u00c4\b\u0003\n\u0003\f\u0003\u00c7\t\u0003\u0001\u0003\u0001"+
		"\u0003\u0005\u0003\u00cb\b\u0003\n\u0003\f\u0003\u00ce\t\u0003\u0001\u0003"+
		"\u0001\u0003\u0001\u0004\u0001\u0004\u0005\u0004\u00d4\b\u0004\n\u0004"+
		"\f\u0004\u00d7\t\u0004\u0001\u0004\u0001\u0004\u0005\u0004\u00db\b\u0004"+
		"\n\u0004\f\u0004\u00de\t\u0004\u0001\u0004\u0001\u0004\u0005\u0004\u00e2"+
		"\b\u0004\n\u0004\f\u0004\u00e5\t\u0004\u0001\u0004\u0001\u0004\u0005\u0004"+
		"\u00e9\b\u0004\n\u0004\f\u0004\u00ec\t\u0004\u0001\u0004\u0005\u0004\u00ef"+
		"\b\u0004\n\u0004\f\u0004\u00f2\t\u0004\u0001\u0004\u0005\u0004\u00f5\b"+
		"\u0004\n\u0004\f\u0004\u00f8\t\u0004\u0001\u0004\u0001\u0004\u0005\u0004"+
		"\u00fc\b\u0004\n\u0004\f\u0004\u00ff\t\u0004\u0001\u0004\u0001\u0004\u0001"+
		"\u0005\u0001\u0005\u0005\u0005\u0105\b\u0005\n\u0005\f\u0005\u0108\t\u0005"+
		"\u0001\u0005\u0001\u0005\u0005\u0005\u010c\b\u0005\n\u0005\f\u0005\u010f"+
		"\t\u0005\u0001\u0005\u0001\u0005\u0005\u0005\u0113\b\u0005\n\u0005\f\u0005"+
		"\u0116\t\u0005\u0001\u0005\u0001\u0005\u0005\u0005\u011a\b\u0005\n\u0005"+
		"\f\u0005\u011d\t\u0005\u0001\u0005\u0005\u0005\u0120\b\u0005\n\u0005\f"+
		"\u0005\u0123\t\u0005\u0001\u0005\u0005\u0005\u0126\b\u0005\n\u0005\f\u0005"+
		"\u0129\t\u0005\u0001\u0005\u0001\u0005\u0005\u0005\u012d\b\u0005\n\u0005"+
		"\f\u0005\u0130\t\u0005\u0001\u0005\u0001\u0005\u0001\u0006\u0001\u0006"+
		"\u0001\u0007\u0001\u0007\u0001\b\u0001\b\u0001\t\u0001\t\u0003\t\u013c"+
		"\b\t\u0001\n\u0001\n\u0001\n\u0000\u0000\u000b\u0000\u0002\u0004\u0006"+
		"\b\n\f\u000e\u0010\u0012\u0014\u0000\u0001\u0001\u0000\u0006\u0007\u015f"+
		"\u0000\u0019\u0001\u0000\u0000\u0000\u0002M\u0001\u0000\u0000\u0000\u0004"+
		"\u007f\u0001\u0000\u0000\u0000\u0006\u00a0\u0001\u0000\u0000\u0000\b\u00d1"+
		"\u0001\u0000\u0000\u0000\n\u0102\u0001\u0000\u0000\u0000\f\u0133\u0001"+
		"\u0000\u0000\u0000\u000e\u0135\u0001\u0000\u0000\u0000\u0010\u0137\u0001"+
		"\u0000\u0000\u0000\u0012\u013b\u0001\u0000\u0000\u0000\u0014\u013d\u0001"+
		"\u0000\u0000\u0000\u0016\u0018\u0005\u0004\u0000\u0000\u0017\u0016\u0001"+
		"\u0000\u0000\u0000\u0018\u001b\u0001\u0000\u0000\u0000\u0019\u0017\u0001"+
		"\u0000\u0000\u0000\u0019\u001a\u0001\u0000\u0000\u0000\u001a\u001c\u0001"+
		"\u0000\u0000\u0000\u001b\u0019\u0001\u0000\u0000\u0000\u001c \u0005\u0001"+
		"\u0000\u0000\u001d\u001f\u0005\u0004\u0000\u0000\u001e\u001d\u0001\u0000"+
		"\u0000\u0000\u001f\"\u0001\u0000\u0000\u0000 \u001e\u0001\u0000\u0000"+
		"\u0000 !\u0001\u0000\u0000\u0000!#\u0001\u0000\u0000\u0000\" \u0001\u0000"+
		"\u0000\u0000#\'\u0005\u0005\u0000\u0000$&\u0005\u0004\u0000\u0000%$\u0001"+
		"\u0000\u0000\u0000&)\u0001\u0000\u0000\u0000\'%\u0001\u0000\u0000\u0000"+
		"\'(\u0001\u0000\u0000\u0000(*\u0001\u0000\u0000\u0000)\'\u0001\u0000\u0000"+
		"\u0000*.\u0005\u0003\u0000\u0000+-\u0005\u000e\u0000\u0000,+\u0001\u0000"+
		"\u0000\u0000-0\u0001\u0000\u0000\u0000.,\u0001\u0000\u0000\u0000./\u0001"+
		"\u0000\u0000\u0000/1\u0001\u0000\u0000\u00000.\u0001\u0000\u0000\u0000"+
		"15\u0005\u000f\u0000\u000024\u0005\u000e\u0000\u000032\u0001\u0000\u0000"+
		"\u000047\u0001\u0000\u0000\u000053\u0001\u0000\u0000\u000056\u0001\u0000"+
		"\u0000\u00006;\u0001\u0000\u0000\u000075\u0001\u0000\u0000\u00008:\u0003"+
		"\u0002\u0001\u000098\u0001\u0000\u0000\u0000:=\u0001\u0000\u0000\u0000"+
		";9\u0001\u0000\u0000\u0000;<\u0001\u0000\u0000\u0000<>\u0001\u0000\u0000"+
		"\u0000=;\u0001\u0000\u0000\u0000>?\u0003\u0004\u0002\u0000?C\u0005\u0010"+
		"\u0000\u0000@B\u0005\u0004\u0000\u0000A@\u0001\u0000\u0000\u0000BE\u0001"+
		"\u0000\u0000\u0000CA\u0001\u0000\u0000\u0000CD\u0001\u0000\u0000\u0000"+
		"DF\u0001\u0000\u0000\u0000EC\u0001\u0000\u0000\u0000FJ\u0005\u0011\u0000"+
		"\u0000GI\u0005\u0004\u0000\u0000HG\u0001\u0000\u0000\u0000IL\u0001\u0000"+
		"\u0000\u0000JH\u0001\u0000\u0000\u0000JK\u0001\u0000\u0000\u0000K\u0001"+
		"\u0001\u0000\u0000\u0000LJ\u0001\u0000\u0000\u0000MQ\u0005\b\u0000\u0000"+
		"NP\u0005\u000e\u0000\u0000ON\u0001\u0000\u0000\u0000PS\u0001\u0000\u0000"+
		"\u0000QO\u0001\u0000\u0000\u0000QR\u0001\u0000\u0000\u0000RT\u0001\u0000"+
		"\u0000\u0000SQ\u0001\u0000\u0000\u0000TX\u0005\f\u0000\u0000UW\u0005\u000e"+
		"\u0000\u0000VU\u0001\u0000\u0000\u0000WZ\u0001\u0000\u0000\u0000XV\u0001"+
		"\u0000\u0000\u0000XY\u0001\u0000\u0000\u0000Y[\u0001\u0000\u0000\u0000"+
		"ZX\u0001\u0000\u0000\u0000[_\u0005\t\u0000\u0000\\^\u0005\u000e\u0000"+
		"\u0000]\\\u0001\u0000\u0000\u0000^a\u0001\u0000\u0000\u0000_]\u0001\u0000"+
		"\u0000\u0000_`\u0001\u0000\u0000\u0000`e\u0001\u0000\u0000\u0000a_\u0001"+
		"\u0000\u0000\u0000bd\u0005\u000b\u0000\u0000cb\u0001\u0000\u0000\u0000"+
		"dg\u0001\u0000\u0000\u0000ec\u0001\u0000\u0000\u0000ef\u0001\u0000\u0000"+
		"\u0000fk\u0001\u0000\u0000\u0000ge\u0001\u0000\u0000\u0000hj\u0005\u000e"+
		"\u0000\u0000ih\u0001\u0000\u0000\u0000jm\u0001\u0000\u0000\u0000ki\u0001"+
		"\u0000\u0000\u0000kl\u0001\u0000\u0000\u0000ln\u0001\u0000\u0000\u0000"+
		"mk\u0001\u0000\u0000\u0000nr\u0005\t\u0000\u0000oq\u0005\u000e\u0000\u0000"+
		"po\u0001\u0000\u0000\u0000qt\u0001\u0000\u0000\u0000rp\u0001\u0000\u0000"+
		"\u0000rs\u0001\u0000\u0000\u0000su\u0001\u0000\u0000\u0000tr\u0001\u0000"+
		"\u0000\u0000uy\u0005\f\u0000\u0000vx\u0005\u000e\u0000\u0000wv\u0001\u0000"+
		"\u0000\u0000x{\u0001\u0000\u0000\u0000yw\u0001\u0000\u0000\u0000yz\u0001"+
		"\u0000\u0000\u0000z\u0003\u0001\u0000\u0000\u0000{y\u0001\u0000\u0000"+
		"\u0000|~\u0003\u0006\u0003\u0000}|\u0001\u0000\u0000\u0000~\u0081\u0001"+
		"\u0000\u0000\u0000\u007f}\u0001\u0000\u0000\u0000\u007f\u0080\u0001\u0000"+
		"\u0000\u0000\u0080\u0085\u0001\u0000\u0000\u0000\u0081\u007f\u0001\u0000"+
		"\u0000\u0000\u0082\u0084\u0005\u000e\u0000\u0000\u0083\u0082\u0001\u0000"+
		"\u0000\u0000\u0084\u0087\u0001\u0000\u0000\u0000\u0085\u0083\u0001\u0000"+
		"\u0000\u0000\u0085\u0086\u0001\u0000\u0000\u0000\u0086\u008b\u0001\u0000"+
		"\u0000\u0000\u0087\u0085\u0001\u0000\u0000\u0000\u0088\u008a\u0003\b\u0004"+
		"\u0000\u0089\u0088\u0001\u0000\u0000\u0000\u008a\u008d\u0001\u0000\u0000"+
		"\u0000\u008b\u0089\u0001\u0000\u0000\u0000\u008b\u008c\u0001\u0000\u0000"+
		"\u0000\u008c\u0091\u0001\u0000\u0000\u0000\u008d\u008b\u0001\u0000\u0000"+
		"\u0000\u008e\u0090\u0005\u000e\u0000\u0000\u008f\u008e\u0001\u0000\u0000"+
		"\u0000\u0090\u0093\u0001\u0000\u0000\u0000\u0091\u008f\u0001\u0000\u0000"+
		"\u0000\u0091\u0092\u0001\u0000\u0000\u0000\u0092\u0097\u0001\u0000\u0000"+
		"\u0000\u0093\u0091\u0001\u0000\u0000\u0000\u0094\u0096\u0003\n\u0005\u0000"+
		"\u0095\u0094\u0001\u0000\u0000\u0000\u0096\u0099\u0001\u0000\u0000\u0000"+
		"\u0097\u0095\u0001\u0000\u0000\u0000\u0097\u0098\u0001\u0000\u0000\u0000"+
		"\u0098\u009d\u0001\u0000\u0000\u0000\u0099\u0097\u0001\u0000\u0000\u0000"+
		"\u009a\u009c\u0005\u000e\u0000\u0000\u009b\u009a\u0001\u0000\u0000\u0000"+
		"\u009c\u009f\u0001\u0000\u0000\u0000\u009d\u009b\u0001\u0000\u0000\u0000"+
		"\u009d\u009e\u0001\u0000\u0000\u0000\u009e\u0005\u0001\u0000\u0000\u0000"+
		"\u009f\u009d\u0001\u0000\u0000\u0000\u00a0\u00a4\u0005\u0012\u0000\u0000"+
		"\u00a1\u00a3\u0005\u001b\u0000\u0000\u00a2\u00a1\u0001\u0000\u0000\u0000"+
		"\u00a3\u00a6\u0001\u0000\u0000\u0000\u00a4\u00a2\u0001\u0000\u0000\u0000"+
		"\u00a4\u00a5\u0001\u0000\u0000\u0000\u00a5\u00a7\u0001\u0000\u0000\u0000"+
		"\u00a6\u00a4\u0001\u0000\u0000\u0000\u00a7\u00ab\u0005\u0019\u0000\u0000"+
		"\u00a8\u00aa\u0005\u001b\u0000\u0000\u00a9\u00a8\u0001\u0000\u0000\u0000"+
		"\u00aa\u00ad\u0001\u0000\u0000\u0000\u00ab\u00a9\u0001\u0000\u0000\u0000"+
		"\u00ab\u00ac\u0001\u0000\u0000\u0000\u00ac\u00ae\u0001\u0000\u0000\u0000"+
		"\u00ad\u00ab\u0001\u0000\u0000\u0000\u00ae\u00b2\u0005\u0015\u0000\u0000"+
		"\u00af\u00b1\u0005\u001b\u0000\u0000\u00b0\u00af\u0001\u0000\u0000\u0000"+
		"\u00b1\u00b4\u0001\u0000\u0000\u0000\u00b2\u00b0\u0001\u0000\u0000\u0000"+
		"\u00b2\u00b3\u0001\u0000\u0000\u0000\u00b3\u00b5\u0001\u0000\u0000\u0000"+
		"\u00b4\u00b2\u0001\u0000\u0000\u0000\u00b5\u00b9\u0005\u001c\u0000\u0000"+
		"\u00b6\u00b8\u0005\u001b\u0000\u0000\u00b7\u00b6\u0001\u0000\u0000\u0000"+
		"\u00b8\u00bb\u0001\u0000\u0000\u0000\u00b9\u00b7\u0001\u0000\u0000\u0000"+
		"\u00b9\u00ba\u0001\u0000\u0000\u0000\u00ba\u00bf\u0001\u0000\u0000\u0000"+
		"\u00bb\u00b9\u0001\u0000\u0000\u0000\u00bc\u00be\u0003\f\u0006\u0000\u00bd"+
		"\u00bc\u0001\u0000\u0000\u0000\u00be\u00c1\u0001\u0000\u0000\u0000\u00bf"+
		"\u00bd\u0001\u0000\u0000\u0000\u00bf\u00c0\u0001\u0000\u0000\u0000\u00c0"+
		"\u00c5\u0001\u0000\u0000\u0000\u00c1\u00bf\u0001\u0000\u0000\u0000\u00c2"+
		"\u00c4\u0005\u001b\u0000\u0000\u00c3\u00c2\u0001\u0000\u0000\u0000\u00c4"+
		"\u00c7\u0001\u0000\u0000\u0000\u00c5\u00c3\u0001\u0000\u0000\u0000\u00c5"+
		"\u00c6\u0001\u0000\u0000\u0000\u00c6\u00c8\u0001\u0000\u0000\u0000\u00c7"+
		"\u00c5\u0001\u0000\u0000\u0000\u00c8\u00cc\u0005\u001d\u0000\u0000\u00c9"+
		"\u00cb\u0005\u001b\u0000\u0000\u00ca\u00c9\u0001\u0000\u0000\u0000\u00cb"+
		"\u00ce\u0001\u0000\u0000\u0000\u00cc\u00ca\u0001\u0000\u0000\u0000\u00cc"+
		"\u00cd\u0001\u0000\u0000\u0000\u00cd\u00cf\u0001\u0000\u0000\u0000\u00ce"+
		"\u00cc\u0001\u0000\u0000\u0000\u00cf\u00d0\u0005\u001e\u0000\u0000\u00d0"+
		"\u0007\u0001\u0000\u0000\u0000\u00d1\u00d5\u0005\u0013\u0000\u0000\u00d2"+
		"\u00d4\u0005%\u0000\u0000\u00d3\u00d2\u0001\u0000\u0000\u0000\u00d4\u00d7"+
		"\u0001\u0000\u0000\u0000\u00d5\u00d3\u0001\u0000\u0000\u0000\u00d5\u00d6"+
		"\u0001\u0000\u0000\u0000\u00d6\u00d8\u0001\u0000\u0000\u0000\u00d7\u00d5"+
		"\u0001\u0000\u0000\u0000\u00d8\u00dc\u0005#\u0000\u0000\u00d9\u00db\u0005"+
		"%\u0000\u0000\u00da\u00d9\u0001\u0000\u0000\u0000\u00db\u00de\u0001\u0000"+
		"\u0000\u0000\u00dc\u00da\u0001\u0000\u0000\u0000\u00dc\u00dd\u0001\u0000"+
		"\u0000\u0000\u00dd\u00df\u0001\u0000\u0000\u0000\u00de\u00dc\u0001\u0000"+
		"\u0000\u0000\u00df\u00e3\u0005\u001f\u0000\u0000\u00e0\u00e2\u0005%\u0000"+
		"\u0000\u00e1\u00e0\u0001\u0000\u0000\u0000\u00e2\u00e5\u0001\u0000\u0000"+
		"\u0000\u00e3\u00e1\u0001\u0000\u0000\u0000\u00e3\u00e4\u0001\u0000\u0000"+
		"\u0000\u00e4\u00e6\u0001\u0000\u0000\u0000\u00e5\u00e3\u0001\u0000\u0000"+
		"\u0000\u00e6\u00ea\u0005&\u0000\u0000\u00e7\u00e9\u0005%\u0000\u0000\u00e8"+
		"\u00e7\u0001\u0000\u0000\u0000\u00e9\u00ec\u0001\u0000\u0000\u0000\u00ea"+
		"\u00e8\u0001\u0000\u0000\u0000\u00ea\u00eb\u0001\u0000\u0000\u0000\u00eb"+
		"\u00f0\u0001\u0000\u0000\u0000\u00ec\u00ea\u0001\u0000\u0000\u0000\u00ed"+
		"\u00ef\u0003\u000e\u0007\u0000\u00ee\u00ed\u0001\u0000\u0000\u0000\u00ef"+
		"\u00f2\u0001\u0000\u0000\u0000\u00f0\u00ee\u0001\u0000\u0000\u0000\u00f0"+
		"\u00f1\u0001\u0000\u0000\u0000\u00f1\u00f6\u0001\u0000\u0000\u0000\u00f2"+
		"\u00f0\u0001\u0000\u0000\u0000\u00f3\u00f5\u0005%\u0000\u0000\u00f4\u00f3"+
		"\u0001\u0000\u0000\u0000\u00f5\u00f8\u0001\u0000\u0000\u0000\u00f6\u00f4"+
		"\u0001\u0000\u0000\u0000\u00f6\u00f7\u0001\u0000\u0000\u0000\u00f7\u00f9"+
		"\u0001\u0000\u0000\u0000\u00f8\u00f6\u0001\u0000\u0000\u0000\u00f9\u00fd"+
		"\u0005\'\u0000\u0000\u00fa\u00fc\u0005%\u0000\u0000\u00fb\u00fa\u0001"+
		"\u0000\u0000\u0000\u00fc\u00ff\u0001\u0000\u0000\u0000\u00fd\u00fb\u0001"+
		"\u0000\u0000\u0000\u00fd\u00fe\u0001\u0000\u0000\u0000\u00fe\u0100\u0001"+
		"\u0000\u0000\u0000\u00ff\u00fd\u0001\u0000\u0000\u0000\u0100\u0101\u0005"+
		"(\u0000\u0000\u0101\t\u0001\u0000\u0000\u0000\u0102\u0106\u0005\u0014"+
		"\u0000\u0000\u0103\u0105\u00051\u0000\u0000\u0104\u0103\u0001\u0000\u0000"+
		"\u0000\u0105\u0108\u0001\u0000\u0000\u0000\u0106\u0104\u0001\u0000\u0000"+
		"\u0000\u0106\u0107\u0001\u0000\u0000\u0000\u0107\u0109\u0001\u0000\u0000"+
		"\u0000\u0108\u0106\u0001\u0000\u0000\u0000\u0109\u010d\u0005/\u0000\u0000"+
		"\u010a\u010c\u00051\u0000\u0000\u010b\u010a\u0001\u0000\u0000\u0000\u010c"+
		"\u010f\u0001\u0000\u0000\u0000\u010d\u010b\u0001\u0000\u0000\u0000\u010d"+
		"\u010e\u0001\u0000\u0000\u0000\u010e\u0110\u0001\u0000\u0000\u0000\u010f"+
		"\u010d\u0001\u0000\u0000\u0000\u0110\u0114\u0005)\u0000\u0000\u0111\u0113"+
		"\u00051\u0000\u0000\u0112\u0111\u0001\u0000\u0000\u0000\u0113\u0116\u0001"+
		"\u0000\u0000\u0000\u0114\u0112\u0001\u0000\u0000\u0000\u0114\u0115\u0001"+
		"\u0000\u0000\u0000\u0115\u0117\u0001\u0000\u0000\u0000\u0116\u0114\u0001"+
		"\u0000\u0000\u0000\u0117\u011b\u0005-\u0000\u0000\u0118\u011a\u00051\u0000"+
		"\u0000\u0119\u0118\u0001\u0000\u0000\u0000\u011a\u011d\u0001\u0000\u0000"+
		"\u0000\u011b\u0119\u0001\u0000\u0000\u0000\u011b\u011c\u0001\u0000\u0000"+
		"\u0000\u011c\u0121\u0001\u0000\u0000\u0000\u011d\u011b\u0001\u0000\u0000"+
		"\u0000\u011e\u0120\u0003\u0010\b\u0000\u011f\u011e\u0001\u0000\u0000\u0000"+
		"\u0120\u0123\u0001\u0000\u0000\u0000\u0121\u011f\u0001\u0000\u0000\u0000"+
		"\u0121\u0122\u0001\u0000\u0000\u0000\u0122\u0127\u0001\u0000\u0000\u0000"+
		"\u0123\u0121\u0001\u0000\u0000\u0000\u0124\u0126\u00051\u0000\u0000\u0125"+
		"\u0124\u0001\u0000\u0000\u0000\u0126\u0129\u0001\u0000\u0000\u0000\u0127"+
		"\u0125\u0001\u0000\u0000\u0000\u0127\u0128\u0001\u0000\u0000\u0000\u0128"+
		"\u012a\u0001\u0000\u0000\u0000\u0129\u0127\u0001\u0000\u0000\u0000\u012a"+
		"\u012e\u0005.\u0000\u0000\u012b\u012d\u00051\u0000\u0000\u012c\u012b\u0001"+
		"\u0000\u0000\u0000\u012d\u0130\u0001\u0000\u0000\u0000\u012e\u012c\u0001"+
		"\u0000\u0000\u0000\u012e\u012f\u0001\u0000\u0000\u0000\u012f\u0131\u0001"+
		"\u0000\u0000\u0000\u0130\u012e\u0001\u0000\u0000\u0000\u0131\u0132\u0005"+
		"2\u0000\u0000\u0132\u000b\u0001\u0000\u0000\u0000\u0133\u0134\u0005\u0018"+
		"\u0000\u0000\u0134\r\u0001\u0000\u0000\u0000\u0135\u0136\u0005\"\u0000"+
		"\u0000\u0136\u000f\u0001\u0000\u0000\u0000\u0137\u0138\u0005,\u0000\u0000"+
		"\u0138\u0011\u0001\u0000\u0000\u0000\u0139\u013c\u0003\u0014\n\u0000\u013a"+
		"\u013c\u0005\u0004\u0000\u0000\u013b\u0139\u0001\u0000\u0000\u0000\u013b"+
		"\u013a\u0001\u0000\u0000\u0000\u013c\u0013\u0001\u0000\u0000\u0000\u013d"+
		"\u013e\u0007\u0000\u0000\u0000\u013e\u0015\u0001\u0000\u0000\u0000+\u0019"+
		" \'.5;CJQX_ekry\u007f\u0085\u008b\u0091\u0097\u009d\u00a4\u00ab\u00b2"+
		"\u00b9\u00bf\u00c5\u00cc\u00d5\u00dc\u00e3\u00ea\u00f0\u00f6\u00fd\u0106"+
		"\u010d\u0114\u011b\u0121\u0127\u012e\u013b";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}