package codeGeneration;

import ast.Node.controller.*;
import ast.PairTest;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.PriorityQueue;

public class GenerateController {
    public static StringBuilder mainString = new StringBuilder();
    static String pathToFileController = "";
    static PriorityQueue<PairTest> pairPriorityQueue = new PriorityQueue<>();

    public static void writeOnFile() {
        while (!pairPriorityQueue.isEmpty()) {
            PairTest pairTest = pairPriorityQueue.poll();
            mainString.append(pairTest.first);
        }
        writeOnFile(pathToFileController, mainString);
    }

    public static void writeOnFile(String pathToFileController, StringBuilder mainString) {
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(pathToFileController));
            writer.write(String.valueOf(mainString));
            writer.close();
        } catch (IOException ioException) {
            System.out.println(ioException.getMessage());
        } finally {

            System.out.println("Done Write on files");
        }
    }

    public static void generateController(HashMap<String, Object> functionsController) {
        for (String key : functionsController.keySet()) {
            if (functionsController.get(key) instanceof Function) {
                pathToFileController = "";
                pairPriorityQueue = new PriorityQueue<>();
                mainString = new StringBuilder();
                pathToFileController = "/Applications/MAMP/htdocs/result/" + key + ".php";
                if (functionsController.get(key) instanceof Function) {
                    writeCodePhp(functionsController.get(key));
                }
                writeOnFile();
            }
        }
    }

    public static void writeCodePhp(Object object) {
        Function function = (Function) object;
        String textCode = "<?php\n";
        pairPriorityQueue.add(new PairTest(textCode, function.getScope().getStartScope()));
        if (function.getBlockStatement() != null || !function.getBlockStatement().isEmpty()) {
            writeBlockStatement(function.getBlockStatement());
        }
    }

    public static void writeBlockStatement(ArrayList<BlockStatement> blockStatements) {
        for (BlockStatement blockStatement :
                blockStatements) {
            if (blockStatement.getPrints() != null) {
                writePrintStatement(blockStatement.getPrints());
            }
            if (blockStatement.getVariables() != null) {
                writeVariableDefine(blockStatement.getVariables());
            }
            if (blockStatement.getIfStatements() != null) {
                writeIfStatement(blockStatement.getIfStatements());
            }
            if (blockStatement.getWriteOnFile() != null) {
                writeStatementWriteOnFile(blockStatement.getWriteOnFile());
            }
        }
    }

    public static void writeStatementWriteOnFile(WriteOnFileData writeOnFile) {
        String text;

        text = "file_put_contents(\"resultFolder/";

        if (writeOnFile.getData() instanceof Variable) {
            text += ((Variable) writeOnFile.getData()).getNameVariable() + "\",";
            text += "$" + ((Variable) writeOnFile.getData()).getNameVariable() + ");\n";

        } else {
            text += writeOnFile.getData() + "\",";
            text += "\"" + writeOnFile.getData() + "\");\n";
        }
        System.out.println(text);
        pairPriorityQueue.add(new PairTest(text, writeOnFile.getScope().getStartScope()));
    }

    public static void writePrintStatement(Print print) {
        String text;
        double bios;

        text = "echo ";
        if (!(print.getTextPrint() instanceof Variable))
            text += "\"" + print.getTextPrint() + "\" ." + "<br> ;";
        else {
            text += "$" + ((Variable) print.getTextPrint()).getNameVariable() + " . \"<br>\";\n";
        }
        bios = print.getScope().getStartScope();
        pairPriorityQueue.add(new PairTest(text, bios));


    }

    public static void writeVariableDefine(Variable variable) {
        double bios;
        String text;

        text = "$" + variable.getNameVariable() + " = ";
        bios = variable.getScope().getStartScope();
        if (variable.getValue() instanceof Expression expression) {
            text += processExpression(expression);
        } else {
            if (variable.getValue() instanceof Variable)
                text += "$" + ((Variable) variable.getValue()).getNameVariable() + ";\n";
            else {
                if (variable.getValue() instanceof ReadFromFile) {
                    text += "trim(file_get_contents(\"" + ((ReadFromFile) variable.getValue()).getNameFile() + "\"));\n";
                } else {
                    char[] d = String.valueOf(variable.getValue()).toCharArray();
                    if (d[0] >= '0' && d[0] <= '9') text += variable.getValue() + ";\n";
                    else text += "\"" + variable.getValue() + "\";\n";
                }
            }
        }
        pairPriorityQueue.add(new PairTest(text, bios));


    }

    public static String processExpression(Expression expression) {
        String res;
        if (expression.getFirst() instanceof Variable) {
            res = "$" + ((Variable) expression.getFirst()).getNameVariable() + " ";
        } else {
            String x = (String) expression.getFirst();
            char[] d = x.toCharArray();
            if (d[0] >= '0' && d[0] <= '9') {
                res = x + " ";
            } else {
                res = "\"" + x + "\" ";
            }
        }

        res += expression.getOperator() + " ";

        if (expression.getSecond() instanceof Variable) {
            res += "$" + ((Variable) expression.getSecond()).getNameVariable() + " ;\n";
        } else {
            String x = (String) expression.getSecond();

            char[] d = x.toCharArray();
            if (d[0] >= '0' && d[0] <= '9') {
                res += x + " ";
            } else {
                res += "\"" + x + "\" ;\n";
            }

        }
        return res;
    }

    public static void writeIfStatement(IfStatement ifStatement) {
        StringBuilder text;
        double bios = ifStatement.getScope().getStartScope();
        text = new StringBuilder("if (");
        text.append(writeConditionCode(ifStatement.getCondition(), ifStatement.getLogicalOperators()));
        text.append(") \n {\n");
        pairPriorityQueue.add(new PairTest(text.toString(), bios));

        for (BlockStatement blockStatement : ifStatement.getBlockStatement()
        ) {
            if (blockStatement.getPrints() != null) {

                writePrintStatement(blockStatement.getPrints());
            }
            if (blockStatement.getVariables() != null) {

                writeVariableDefine(blockStatement.getVariables());
            }
            if (blockStatement.getIfStatements() != null) {

                writeIfStatement(blockStatement.getIfStatements());
            }
            if (blockStatement.getWriteOnFile() != null) {
                writeStatementWriteOnFile(blockStatement.getWriteOnFile());
            }
        }
        text = new StringBuilder();
        bios = ifStatement.getScope().getEndScope();
        text.append("}\n");
        pairPriorityQueue.add(new PairTest(text.toString(), bios));
    }

    public static String writeConditionCode(ArrayList<Condition> conditions, ArrayList<String> logicalOperators) {
        StringBuilder text = new StringBuilder();
        int i = 0;
        for (Condition condition :
                conditions) {
            Expression expression = condition.getExpression();
            if (conditions.size() > 1) {
                text.append(" ( ");
            }
            text.append(getOnePart(expression.getFirst()));
            text.append(expression.getOperator()).append(" ");
            text.append(getOnePart(expression.getSecond()));
            if (conditions.size() > 1) {
                text.append(" ) ");
            }
            if (i < conditions.size() - 1) {
                if (logicalOperators.get(i) != null) {
                    text.append(logicalOperators.get(i)).append(" ");
                    i++;
                }
            }

        }
        return text.toString();
    }

    public static String getOnePart(Object object) {
        String text;
        if (object instanceof Variable) {
            text = "$" + ((Variable) object).getNameVariable() + " ";
        } else {
            String temp = (String) object;
            char[] d = temp.toCharArray();
            if (d[0] >= '0' && d[0] <= '9') {
                text = temp + " ";
            } else {
                text = "\"" + temp + "\" ";
            }
        }
        return text;
    }
}
