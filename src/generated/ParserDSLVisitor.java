// Generated from /Users/ahmadabotrab/Desktop/Compiler_2_Project/dsl/src/ParserDSL.g4 by ANTLR 4.10.1
package generated;

import org.antlr.v4.runtime.tree.ParseTreeVisitor;

/**
 * This interface defines a complete generic visitor for a parse tree produced
 * by {@link ParserDSL}.
 *
 * @param <T> The return type of the visit operation. Use {@link Void} for
 *            operations with no return type.
 */
public interface ParserDSLVisitor<T> extends ParseTreeVisitor<T> {
    /**
     * Visit a parse tree produced by {@link ParserDSL#doc}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitDoc(ParserDSL.DocContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#page}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitPage(ParserDSL.PageContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#icon}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitIcon(ParserDSL.IconContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#title}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitTitle(ParserDSL.TitleContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#attributeInPage}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitAttributeInPage(ParserDSL.AttributeInPageContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#navbar}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitNavbar(ParserDSL.NavbarContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#body}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitBody(ParserDSL.BodyContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#footer}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitFooter(ParserDSL.FooterContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#bodyNavBar}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitBodyNavBar(ParserDSL.BodyNavBarContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#bodyBody}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitBodyBody(ParserDSL.BodyBodyContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#bodyFooter}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitBodyFooter(ParserDSL.BodyFooterContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#styleNavBar}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitStyleNavBar(ParserDSL.StyleNavBarContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#styleBody}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitStyleBody(ParserDSL.StyleBodyContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#form}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitForm(ParserDSL.FormContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#attributeForm}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitAttributeForm(ParserDSL.AttributeFormContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#action}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitAction(ParserDSL.ActionContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#method}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitMethod(ParserDSL.MethodContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#target}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitTarget(ParserDSL.TargetContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#autoComplete}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitAutoComplete(ParserDSL.AutoCompleteContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#bodyForm}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitBodyForm(ParserDSL.BodyFormContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#button}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitButton(ParserDSL.ButtonContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#gotoRule}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitGotoRule(ParserDSL.GotoRuleContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#onPressedRule}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitOnPressedRule(ParserDSL.OnPressedRuleContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#radio}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitRadio(ParserDSL.RadioContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#dateTime}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitDateTime(ParserDSL.DateTimeContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#password}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitPassword(ParserDSL.PasswordContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#textFieldRule}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitTextFieldRule(ParserDSL.TextFieldRuleContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#value}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitValue(ParserDSL.ValueContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#hintR}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitHintR(ParserDSL.HintRContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#controllerRule}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitControllerRule(ParserDSL.ControllerRuleContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#variableDefine}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitVariableDefine(ParserDSL.VariableDefineContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#expression}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitExpression(ParserDSL.ExpressionContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#ifCondition}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitIfCondition(ParserDSL.IfConditionContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#conditionStatment}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitConditionStatment(ParserDSL.ConditionStatmentContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#firstParty}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitFirstParty(ParserDSL.FirstPartyContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#secondParty}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitSecondParty(ParserDSL.SecondPartyContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#forStatment}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitForStatment(ParserDSL.ForStatmentContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#blockStatment}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitBlockStatment(ParserDSL.BlockStatmentContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#printStatment}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitPrintStatment(ParserDSL.PrintStatmentContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#function}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitFunction(ParserDSL.FunctionContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#writeOnFileData}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitWriteOnFileData(ParserDSL.WriteOnFileDataContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#readFromFileData}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitReadFromFileData(ParserDSL.ReadFromFileDataContext ctx);

    /**
     * Visit a parse tree produced by {@link ParserDSL#parameter}.
     *
     * @param ctx the parse tree
     * @return the visitor result
     */
    T visitParameter(ParserDSL.ParameterContext ctx);
}