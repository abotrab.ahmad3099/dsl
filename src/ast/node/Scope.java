package ast.Node;

import java.util.Objects;

public class Scope {
    int startScope;
    int endScope;

    public Scope(int startScope, int endScope) {
        this.startScope = startScope;
        this.endScope = endScope;
    }

    public Scope() {
    }

    public int getStartScope() {
        return startScope;
    }

    public void setStartScope(int startScope) {
        this.startScope = startScope;
    }

    public int getEndScope() {
        return endScope;
    }

    public void setEndScope(int endScope) {
        this.endScope = endScope;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Scope scope)) return false;
        return getStartScope() == scope.getStartScope() && getEndScope() == scope.getEndScope();
    }

    @Override
    public int hashCode() {
        return Objects.hash(getStartScope(), getEndScope());
    }

    @Override
    public String toString() {
        return "Scope{" +
                "startScope=" + startScope +
                ", endScope=" + endScope +
                "}\n";
    }
}
