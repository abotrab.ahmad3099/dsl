lexer grammar  LexerDSL;


// Basic for each page is seprate for 3 part ( NavBar - body - Footer)
PAGE
    :'page'

    ;
NAMEPAGE
        :TAG_NameChar
        ;
PAGEOPEN
        : NAMEPAGE+ -> pushMode(page)
        ;

AttributDefault
            :ATTRIBUTE
            ;
OperationDefault
                : ATTCHAR
                ;
HTML_COMMENT
    : '/*' .*? '*/'
    ;
HTML_CONDITIONAL_COMMENT
    : '//'
    ;
DotIn_default : Dot;

Button : 'button' -> pushMode(button);
////Mode Page root of page
mode page;
TITLE
     :'title'
     ;
Icon : 'icon';
DotIn_page : Dot;
SingleQuatePage
            : SingleQuate
            ;
DoubleQuatePage
            : DoubleQuate
            ;
NameClass
        :TAG_NameChar
        ;
TextInPage
        : TAG_NameChar
        ;
OperationInPage
            :ATTCHAR
            ;
DigitInPage
            :DIGIT
            ;

AttributeInPage
            : ATTRIBUTE
            ;

OpenBracket_PAGE
                :OpenBracket
                ;
CloseBracket_PAGE
                :CloseBracket
                ;

NAVBAR
    :'navbar' -> pushMode(navBar)
    ;
BODY
    :'body' -> pushMode(body)
    ;
FOOTER
    :'footer' -> pushMode(footer)
    ;
Controller
    : 'controller' -> pushMode(controller)
    ;

SimiColonPage
    :';' -> popMode
    ;


////mode of navbar
mode navBar;
DotIn_navBar: Dot;
NAVBAR_inMode
            : 'NavBar'
            | 'navBar'
            | 'navbar'
            | 'Navbar'
            | 'NAVBAR'
            ;
SingleQuateNav
            : SingleQuate
            ;
DoubleQuateNav
            : DoubleQuate
            ;
NameClassNav
            :  TAG_NameChar
            ;
TextInNav
        :TAG_NameChar
        ;
OperationInNav
             : ATTCHAR
             ;
DigitInNav
        :DIGIT
        ;
AttributeInNav
            : ATTRIBUTE
            ;
OpenBracket_NAV
                :OpenBracket
                ;
CloseBracket_NAV
                :CloseBracket
                ;
LinkNav
    : LINK
    ;
WidthNav
    : WIDTH
    ;
HightNav
    : HIGHT
    ;
URLNav
    : URL
    ;

StyleNavBar:'style'->pushMode(style);

OpenSquarBracket
                : '[' -> pushMode(array)
                ;
SimiColonNav
            :';' -> popMode
            ;


mode array;
Element
        :'element'
        ;
ColonInArray
            : ':'
            ;
TextNameElement
               : TAG_NameChar
               ;
OpenBracket
          : '('
          ;
CloseBracket
            :')'
            ;
CloseSquarBracket
                : ']' -> popMode
                ;
////mode of body
mode body;
Center:'center';
DotIn_body : Dot;
BodyInMode
        : 'Body'
        | 'body'
        | 'BODY'
        | 'BOdy'
        ;
SingleQuateBody
            : SingleQuate
            ;
DoubleQuateBody
            : DoubleQuate
            ;
NameClassBody
            :  TAG_NameChar
            ;
TextInBody
            : TAG_NameChar
            ;
OperationInBody
            : ATTCHAR
            ;
DigitInBody
            : DIGIT
            ;
AttributeInBody
            : ATTRIBUTE
            ;
OpenBracket_BODY
                :OpenBracket
                ;
CloseBracket_BODY
                :CloseBracket
                ;
StyleBody :'style'->pushMode(style);
Form
          : 'form' -> pushMode(form)
          ;
SimiColonBody
             : ';' ->popMode
             ;

//
////mode of footer
mode footer;
DotIn_footer : Dot;
FOOTERNINMODE
        : 'Footer'
        | 'footer'
        | 'FOOTER'
        ;

SingleQuateFooter
                : SingleQuate
                ;
DoubleQuateFooter
                :  DoubleQuate
                ;
NameClassFooter
            :  TAG_NameChar
            ;
TextInFooter
            : TAG_NameChar
            ;
OpenBracket_FOOTER
                :OpenBracket
                ;
CloseBracket_FOOTER
                :CloseBracket
                ;
OperationInFooter
                : ATTCHAR
                ;
DigitInFooter
            :DIGIT
            ;
AttributeInFooter
            : ATTRIBUTE
            ;
StyleFooter :'style'->pushMode(style);
SIMICOLON_inModeFooter
                :';'->popMode
                ;


mode form;
DotIn_form : Dot;

Method : 'method' -> pushMode(method);
Target : 'target' -> pushMode(target);
AutoComplete : 'autocomplete' -> pushMode(autocomplete);
Action : 'action' -> pushMode(action);

//   text field many type writing  with zero experiance develop
TextFieldSmall
            : 'textfield'  -> pushMode(textField)
            ;
TextFieldSmallAndLarge
            : 'textField' -> pushMode(textField)
            ;
TextFieldLarge
            : 'TextField' -> pushMode(textField)
            ;
TextFieldXLarge
            : 'TEXTFIELD' -> pushMode(textField)
            ;

RadioButton
            : 'radio' -> pushMode(radio)
            ;
Password
            : 'password' -> pushMode(password)
            ;
DateTime  : 'dateTime' -> pushMode(dateTime)
          ;


FormButton : 'button' -> pushMode(button);
//--------------------------------------------------
NameClassForm
            :  TAG_NameChar
            ;
TextInForm
        :TAG_NameChar
        ;
OperationInForm
             : ATTCHAR
             ;
DigitInForm
        :DIGIT
        ;
AttributeInForm
            : ATTRIBUTE
            ;
OpenBracketForm
                :OpenBracket
                ;
CloseBracketForm
                :CloseBracket
                ;
SIMICOLON_inModeForm
                :';'->popMode
                ;

mode radio;
DoubleQuateRadio: '"';
SingleQuateRadio: '\'';
NameRadio : TAG_NameChar;
OperatorInRadio:ATTCHAR;
AttributRadio : ATTRIBUTE;
SimicolonRadio : ';' -> popMode;

mode dateTime;
DoubleQuatedateTime: '"';
SingleQuatedateTime: '\'';
NamedateTime : TAG_NameChar;
OperatorIndateTime:ATTCHAR;
AttributdateTime : ATTRIBUTE;
SimicolondateTime : ';' -> popMode;

mode password;
DoubleQuatepassword: '"';
SingleQuatepassword: '\'';
Namepassword : TAG_NameChar;
OperatorInpassword:ATTCHAR;
Attributpassword : ATTRIBUTE;
Simicolonpassword : ';' -> popMode;

mode button;
DoubleQuateButton: '"';
SingleQuateButton: '\'';
NameButton : TAG_NameChar;
OperatorInButton:ATTCHAR;
SimpleTextButton:TAG_NameChar;
AttributButton : ATTRIBUTE;
ActionOnclick : 'on click' ;
ActionOnchange : 'on change' ;
ActionOnMouseover : 'on mouseOver';
GoTo : 'go to';
TextButton : TAG_NameChar;
SimicolonButton : ';' -> popMode;


mode textField;
Lable
    : 'lable' -> pushMode(lable)
    ;
DoubleQuateTextField: '"';
SingleQuateTextField: '\'';
NameFieldInput : TAG_NameChar;
OperatorInTextField:ATTCHAR;
SimpleText:TAG_NameChar;
OpenBracketTextField
                :OpenBracket
                ;
CloseBracketTextField
                :CloseBracket
                ;
AttributTextField : ATTRIBUTE;
Value : 'value' -> pushMode(value);
HintText : 'hint' -> pushMode(hint);
SimicolonTextField : ';' -> popMode;

mode hint;
AttributHint : ATTRIBUTE;
EqualHint
    : ATTCHAR
    ;
TextCharHint: TAG_NameChar;
SimiColonHint: ';' -> popMode;


mode value;
DoubleQuateValue: '"';
SingleQuateValue: '\'';
EqualValue
    : ATTCHAR
    ;
TextCharValue : TAG_NameChar;
AttributeInValue
            : ATTRIBUTE
            ;
SimicolonValue : ';' -> popMode;

mode method;
DotIn_method : Dot;
EqualMethod
    : ATTCHAR
    ;
AttributeInMethod
            : ATTRIBUTE
            ;

DoubleQuateMethod : '"';
SingleQuateMethod : '\'';
GET
    : 'get'
    | 'GET'
    | 'Get'
    ;
POST
    : 'post'
    | 'POST'
    | 'Post'
    ;
PUT
    : 'put'
    | 'PUT'
    | 'Put'
    ;
SimicolonMethod : ';' -> popMode;


mode target;
DotIn_target : Dot;

EqualTarget
    : ATTCHAR
    ;
AttributeInTarget
            : ATTRIBUTE
            ;
SimicolonTarget : ';' -> popMode;
DoubleQuateTarget : DoubleQuate;
SingleQuateTarget : SingleQuate;
Blank : 'blank' | '_blank' | '_Blank' | '_BLANK' | 'BLANK' ;
Self : 'self' | '_self';
Parent : 'parent' | '_parent';
Top : 'top' | '_top';

mode autocomplete;
AttributeInAuto
            : ATTRIBUTE
            ;
EqualAuto
    : ATTCHAR
    ;
SimicolonAuto : ';' -> popMode;
DoubleQuateAuto : DoubleQuate;
SingleQuateAuto : SingleQuate;
On:'on';
Off:'off';


mode type;
DotIn_type : Dot;
NumberType
    : 'number'
    ;
TextType
    : 'text'
    ;
Radio:
    'radio'
    ;
CheckBox
    : 'checkBox'
    ;

Date
    : 'date'
    ;
Equal
    : '='
    ;
SimiColonType
    : ';'->popMode
    ;


mode action;

EqualAction
    : ATTCHAR
    ;
AttributeInAction
            : ATTRIBUTE
            ;
DoubleQuationAction
    : '"'
    ;
SingleQuationAction
    : '\''
    ;
TextCharAction : TAG_NameChar;
SimiColonAction
    : ';' -> popMode
    ;


mode controller;
WriteFile : 'writefile' -> pushMode(writefile);

Text : (TAG_NameChar | '_' ) ;
Numbers : Digit;
SimicolonEnd : ';';
DoubleQuateController : DoubleQuate;
SingleQuateController : SingleQuate;
IF : 'if';
ELSE : 'eles';
OpenQurlyBracket : '{';
CloseQurlyBracket : '}';
OpenSquarBracketController :'[';
ColseSquarBracketController :']';
EqualController : '==' ;
Assignment : '=' ->pushMode(secondParty) ;
EqualValueAndType : '===';
For : 'for';
While : 'while';
OpenBracketController : '(';
CloseBracketController : ')';
OpenFile : 'openFile';
Null : 'null' | 'Null' | 'NULL' ;
Asset : 'asset';
Function : 'function';
Get : 'get' | 'GET' | 'Get';
Post : 'post' | 'Post' | 'POST' ;
SpacesOrEndLineOrTab : ATTRIBUTE;
OperatorNeeded : ATTCHAR;
And : 'and' | 'And' | 'AND' ;
Or : ' or ' | ' OR ' | ' Or ';
Not : 'not' | 'Not' | 'NOT';
ColonController : ':';
Print : 'print' ;
Return : 'return';
ADD : '+';
Minus : '-';
Multipule: '*';
Divid : '/';
EndController : 'endController'->popMode;
IN : ' in';

mode secondParty;
ReadFile : 'readfile' -> pushMode(readfile);
WhiteSpaceSecond : '\t' | '\n' | ' ';
TextNameVariable : (TAG_NameChar | '_' ) ;
DoubleQuateSecond : DoubleQuate;
SingleQuateSecond : SingleQuate;
NumbersSecondParty : Digit;
ADD2 : '+' -> pushMode(express);
Minus2 : '-' -> pushMode(express);
Multipule2: '*' -> pushMode(express);
Divid2 : '/' ->pushMode(express);
SimicolonSecond : ';' -> popMode ;

mode express;

TEXTSECOND : (TAG_NameChar | '_' );
WhiteSpaceSecondSecond : '\t' | '\n' | ' ';
DoubleQuateSecondSecond : DoubleQuate;
SingleQuateSecondSecond  : SingleQuate;
SimicolonSecondSecond : ';' -> popMode,popMode;

mode conditionSecondParty;
TextConditionSecondParty : (TAG_NameChar | '_' );
DoubleQuateConditionSecondParty: DoubleQuate;
SingleQuateConditionSecondParty : SingleQuate;
OneWhiteCondition : ' ' ;
WhiteSpaceConditionSecondParty : '  '->popMode;

mode lable;
OperationInLable : ATTCHAR ;
TextCharLable : TAG_NameChar;
DoubleQuateLable : DoubleQuate;
SingleQuateLable : SingleQuate;
AttributeInLable
            : ATTRIBUTE
            ;
SimiColonLable
        : ';' -> popMode
        ;
mode writefile;
Namewritefile : TAG_NameChar | ' ' | '\n';
DoubleQuatewritefile: DoubleQuate;
SingleQuatewritefile: SingleQuate;
Spaceswritefile : '\t' | '\n' | ' ';
Operatorwritefile : ATTCHAR;
SimiColonwritefile : ';' ->popMode;


mode readfile;
Namereadfile : TAG_NameChar;
DoubleQuatereadfile: DoubleQuate;
SingleQuatereadfile: SingleQuate;
Spacesreadfile : '\t' | '\n' | ' ';
Operatorreadfile : ATTCHAR;
SimiColonreadfile : ';' ->popMode,popMode;


/////________________________________Style mode ___________________________________
mode style;
DotIn_style : Dot;
ColonStyle
        : ATTCHAR
        ;
DoubleQuateStyle
        :DoubleQuate
        ;
SingleQuateStyle
        :SingleQuate
        ;
Color
    : 'color'->pushMode(color)
    ;
FontFamily
    : 'font-size' -> pushMode(fontSize)
    ;
FontSize
    : 'font-family' -> pushMode(fontFamily)
    ;
TextAlign
    :'text-align'->pushMode(textAlign)
    ;
H1: 'h1' ->pushMode(h1);
H2: 'h2' ->pushMode(h2);
H3: 'h3' ->pushMode(h3);
H4: 'h4' ->pushMode(h4);
H5: 'h5' ->pushMode(h5);
H6: 'h6' ->pushMode(h6);

Background : 'background';
BackgroundColor : 'background-color';


//color is
mode color;
Red : 'red';
Yellow : 'yellow';
Blue : 'blue';
Green : 'green';
White : 'white';
Black : 'black';
Purple: 'purple';
DigitForColorHex : ATTCHAR;
TextForColorHex : TAG_NameChar;
SimiColonColor
         : ';' -> popMode;

mode fontFamily;
TextFontName
        : TAG_NameChar
        ;
SimiColonFontFamily
         : ';' -> popMode;

mode fontSize;
Digit
    : DIGIT
    ;
SimiColonFontSize
         : ';' -> popMode
         ;
mode textAlign;
SimiColonTextAlign
         : ';' -> popMode
         ;


mode h1;
Colon_h1
        : ':'
        ;
DotIn_h1 : Dot;
NameClassH1
        : TAG_NameChar
        ;
SingleQuateH1
        : '\''
        ;
ContentH1
        :TAG_NameChar
        ;
SimiColonH1
        : ';' ->popMode
        ;

mode h2;
DotIn_h2 : Dot;
Colon_h2
        : ':'
        ;
NameClassH2
        : TAG_NameChar
        ;
SingleQuateH2
        : '\''
        ;
ContentH2
        :TAG_NameChar
        ;
SimiColonH2
        : ';' ->popMode
        ;

mode h3;
DotIn_h3 : Dot;
Colon_h3
        : ':'
        ;
NameClassH3
        : TAG_NameChar
        ;
SingleQuateH3
        : '\''
        ;
ContentH3
        :TAG_NameChar
        ;
SimiColonH3
        : ';' ->popMode
        ;
mode h4;
DotIn_h4 : Dot;
Colon_h4
        : ':'
        ;
NameClassH4
        : TAG_NameChar
        ;
SingleQuateH4
        : '\''
        ;
ContentH4
        :TAG_NameChar
        ;
SimiColonH4
        : ';' ->popMode
        ;
mode h5;
DotIn_h5 : Dot;
Colon_h5
       : ':'
       ;
NameClassH5
          : TAG_NameChar
          ;
SingleQuateH5
            : '\''
            ;
ContentH5
         :TAG_NameChar
         ;
SimiColonH5
           : ';' ->popMode
           ;
mode h6;
DotIn_h6 : Dot;
Colon_h6
        : ':'
        ;
NameClassH6
        : TAG_NameChar
        ;
SingleQuateH6
        : '\''
        ;
ContentH6
        :TAG_NameChar
        ;
SimiColonH6
        : ';' ->popMode
        ;



fragment
ATTCHAR
    : '_'
    | '.'
    | '?'
    | ':'
    | ','
    | '#'
    | '@'
//    | SingleQuate
//    | DoubleQuate
    ;

fragment
DIGIT
    : [0-9]
    ;





fragment
TAG_NameChar
    : TAG_NameStartChar
    | DIGIT
    ;
//fragment

fragment
TAG_NameStartChar
    : [a-zA-Z]
    ;

fragment
ATTRIBUTE
        :'\n'
        |'\t'
        | WhiteSpace
        ;

fragment
WhiteSpace
        :' '
        ;

fragment
DoubleQuate
           : '"'
           ;
fragment
SingleQuate
           : '\''
           ;
fragment
Dot
    :'.'
    ;

fragment
SIMICOLON: ';';

fragment
LINK
   : 'link'
   ;
fragment
WIDTH
    : 'width'
    ;
fragment
HIGHT
    : 'hight'
    ;
fragment
URL
   :'url'
   ;
