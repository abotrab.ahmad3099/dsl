package ast.Node.Body.Form.Attribute;

import ast.Node.ParentNode;
import ast.Node.Scope;

public class AutoComplete extends ParentNode {
    String state;


    public AutoComplete() {
    }

    public AutoComplete(String state, Scope scope) {
        this.state = state;
        super.setScope(scope);
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }


}
