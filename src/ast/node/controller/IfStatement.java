package ast.Node.controller;

import ast.Node.ParentNode;
import ast.Node.Scope;

import java.util.ArrayList;

public class IfStatement extends ParentNode {

    String textIf;
    ArrayList<Condition> condition;
    ArrayList<String> logicalOperators;
    ArrayList<BlockStatement> blockStatements;

    public IfStatement() {
        condition = new ArrayList<>();
        logicalOperators = new ArrayList<>();
        blockStatements = new ArrayList<>();
    }

    public IfStatement(Scope scope, String textIf, ArrayList<Condition> condition, ArrayList<BlockStatement> blockStatements) {
        super.setScope(scope);
        this.textIf = textIf;
        this.condition = condition;
        this.blockStatements = blockStatements;
    }

    public IfStatement(Scope scope, ArrayList<Condition> condition, ArrayList<BlockStatement> blockStatements) {
        super.setScope(scope);
        this.condition = condition;
        this.blockStatements = blockStatements;
    }

    public IfStatement(Scope scope, String textIf, ArrayList<Condition> condition, ArrayList<String> logicalOperators, ArrayList<BlockStatement> blockStatements) {
        super.setScope(scope);
        this.textIf = textIf;
        this.condition = condition;
        this.logicalOperators = logicalOperators;
        this.blockStatements = blockStatements;
    }

    public ArrayList<Condition> getCondition() {
        return condition;
    }

    public void setCondition(ArrayList<Condition> condition) {
        this.condition = condition;
    }

    public ArrayList<BlockStatement> getBlockStatement() {
        return blockStatements;
    }

    public void setBlockStatement(ArrayList<BlockStatement> blockStatement) {
        this.blockStatements = blockStatement;
    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }

    public String getTextIf() {
        return textIf;
    }

    public void setTextIf(String textIf) {
        this.textIf = textIf;
    }

    public ArrayList<String> getLogicalOperators() {
        return logicalOperators;
    }

    public void setLogicalOperators(ArrayList<String> logicalOperators) {
        this.logicalOperators = logicalOperators;
    }

    public ArrayList<BlockStatement> getBlockStatements() {
        return blockStatements;
    }

    public void setBlockStatements(ArrayList<BlockStatement> blockStatements) {
        this.blockStatements = blockStatements;
    }


}
