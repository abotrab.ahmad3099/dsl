package ast.Node.controller;

import java.util.Objects;

public class Condition {
    Expression expression;

    public Condition() {
        expression = new Expression();
    }

    public Condition(Expression expression) {
        this.expression = expression;
    }

    public Expression getExpression() {
        return expression;
    }

    public void setExpression(Expression expression) {
        this.expression = expression;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Condition condition)) return false;
        return getExpression().equals(condition.getExpression());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getExpression());
    }

    @Override
    public String toString() {
        return "Condition{" +
                "expression=" + expression +
                '}';
    }
}
