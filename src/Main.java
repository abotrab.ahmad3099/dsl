import ErrorHandling.ErrorSyntax;
import ErrorHandling.StoreError;
import ast.visitor.BaseVisitor;
import codeGeneration.CodeGenerationImp;
import generated.LexerDSL;
import generated.ParserDSL;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.misc.ParseCancellationException;
import org.antlr.v4.runtime.tree.ParseTree;
import symbol_Table.SymbolTable;

import java.io.IOException;

import static org.antlr.v4.runtime.CharStreams.fromFileName;

public class Main {
    public static void main(String[] args) {
        try {
            String source = "src/examples/sample2";
            CharStream cs = fromFileName(source);
            LexerDSL lexer = new LexerDSL(cs);
            try {
                lexer.removeErrorListeners();
                lexer.addErrorListener(ErrorSyntax.INSTANCE);
                CommonTokenStream token = new CommonTokenStream(lexer);
                ParserDSL parser = new ParserDSL(token);
                try {
                    parser.removeErrorListeners();
                    parser.addErrorListener(ErrorSyntax.INSTANCE);
                } catch (ParseCancellationException e) {
                    System.out.println("you have some syntax error please go to ( src/ErrorFilesOutput/syntaxError ) ");
                }
                ParseTree tree = parser.doc();

                if (StoreError.errorList.isEmpty()) {
                    SymbolTable symbolTable = (SymbolTable) new BaseVisitor().visit(tree);
                    CodeGenerationImp codeGenerationImp = new CodeGenerationImp();
                    codeGenerationImp.generate(symbolTable);
                } else {
                    ErrorSyntax.writeErrorsOnFile();
                }

            } catch (ParseCancellationException e) {
                System.out.println("you have some syntax error please go to ( src/ErrorFilesOutput/syntaxError ) ");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}