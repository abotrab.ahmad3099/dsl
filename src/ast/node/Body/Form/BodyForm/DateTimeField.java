package ast.Node.Body.Form.BodyForm;

import ast.Node.ParentNode;
import ast.Node.Scope;

import java.util.Objects;

public class DateTimeField extends ParentNode {
    Scope scope;
    String nameDateTimeField;

    public DateTimeField() {
    }

    public DateTimeField(Scope scope, String nameDateTimeField) {
        super.setScope(scope);
        this.nameDateTimeField = nameDateTimeField;
    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }

    public String getNameDateTimeField() {
        return nameDateTimeField;
    }

    public void setNameDateTimeField(String nameDateTimeField) {
        this.nameDateTimeField = nameDateTimeField;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof DateTimeField that)) return false;
        return getScope().equals(that.getScope()) && getNameDateTimeField().equals(that.getNameDateTimeField());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getScope(), getNameDateTimeField());
    }

    @Override
    public String
    toString() {
        return "DateTimeField{" +
                "scope=" + scope +
                ", nameDateTimeField='" + nameDateTimeField + '\'' +
                '}';
    }
}
