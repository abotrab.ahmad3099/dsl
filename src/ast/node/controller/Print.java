package ast.Node.controller;

import ast.Node.ParentNode;
import ast.Node.Scope;

public class Print extends ParentNode {
    Object textPrint;


    public Print() {
    }

    public Print(String textPrint, Scope scope) {
        this.textPrint = textPrint;
        super.setScope(scope);
    }

    public Object getTextPrint() {
        return textPrint;
    }

    public void setTextPrint(Object textPrint) {
        this.textPrint = textPrint;
    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }


}
