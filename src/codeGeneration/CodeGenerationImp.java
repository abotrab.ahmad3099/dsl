package codeGeneration;

import ErrorHandling.StoreError;
import ast.Node.Body.Body;
import ast.Node.Body.BodyBody;
import ast.Node.Body.Form.Attribute.*;
import ast.Node.Body.Form.BodyForm.*;
import ast.Node.Body.Form.Form;
import ast.Node.Doc;
import ast.Node.Page.AttributeInPage;
import ast.Node.Page.Icon;
import ast.Node.Page.Page;
import ast.Node.Page.Title;
import ast.Node.Scope;
import ast.PairTest;
import symbol_Table.SymbolTable;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Objects;
import java.util.PriorityQueue;

public class CodeGenerationImp {
    StringBuilder mainString = new StringBuilder();
    String fullPath;
    PriorityQueue<PairTest> pairPriorityQueue = new PriorityQueue<>();

    public void writeOnFile() {
        while (!this.pairPriorityQueue.isEmpty()) {
            PairTest pairTest = pairPriorityQueue.poll();
            mainString.append(pairTest.first);
        }
        GenerateController.writeOnFile(fullPath, mainString);
    }

    public void generatedAllPagesInDoc(SymbolTable symbolTable) {
        Doc doc = (Doc) symbolTable.getSymbols().get(Doc.class.getSimpleName());
        for (Page page : doc.getPages()
        ) {
            this.pairPriorityQueue.clear();
            this.pairPriorityQueue = new PriorityQueue<>();
            mainString = new StringBuilder();
            fullPath = "";
            createFileHtml(page, symbolTable);

            writeOnFile();
        }
    }

    public void createFileHtml(Page page, SymbolTable symbolTable) {
        Scope scope = page.getScope();
        String first = """
                <!doctype html>
                <html lang="en">
                """;
        this.pairPriorityQueue.add(new PairTest(first, scope.getStartScope()));

        first = "\n</html>";
        this.pairPriorityQueue.add(new PairTest(first, scope.getEndScope()));

        String fileName = page.getNamePage() + ".html";
        this.fullPath = "/Applications/MAMP/htdocs/result/" + fileName;
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(fullPath));
            writer.close();

            if (page.getTitle() != null) {
                if (page.getIcon() != null) {
                    addTitle(page.getTitle(), page.getIcon());
                } else {
                    addTitle(page.getTitle());
                }
            }
            if (page.getAttributeInPage() != null) {
                AttributeInPage attributeInPage = page.getAttributeInPage();


                if (attributeInPage.getBody() != null) {
                    appendToHtmlFile_Body(attributeInPage.getBody());
                }
                if (!symbolTable.getSymbolTableController().isEmpty()) {
                    GenerateController.generateController(symbolTable.getSymbolTableController());
                }
            }

        } catch (IOException ioException) {
            System.out.println(ioException.getMessage());
        }
    }


    public void addTitle(Title title, Icon icon) {

        Scope scope = title.getScope();

        String first = """
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport"
                          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
                    <meta http-equiv="X-UA-Compatible" content="ie=edge">
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
                    <link rel="stylesheet" href="style.css">
                    """;
        first += "\t<link href=\"";
        first += icon.getPathToIcon() + ".jpg\"";
        first += " rel=\"icon\">\n\t";
        first += "<title>";
        first += title.getTitleString();
        first += """
                </title>
                </head>
                """;
        this.pairPriorityQueue.add(new PairTest(first, scope.getStartScope()));

    }

    public void addTitle(Title title) {

        Scope scope = title.getScope();

        String first = """
                <head>
                    <meta charset="UTF-8">
                    <meta name="viewport"
                          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
                    <meta http-equiv="X-UA-Compatible" content="ie=edge">
                    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
                    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.6.0/jquery.min.js"></script>
                    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
                    <link rel="stylesheet" href="style.css">
                    <title>""";
        first += title.getTitleString();
        first += """
                </title>
                </head>
                """;
        this.pairPriorityQueue.add(new PairTest(first, scope.getStartScope()));

    }

//    public void addNavBar(NavBar navBar) {
//        Scope scope = navBar.getScope();
//
//        if (scope.getStartScope() == scope.getEndScope()) {
//            String first = """
//                        <header class="header-area header-sticky">
//                            <div class="container">
//                                <div class="row">
//                                    <div class="col-12">
//                                        <nav class="main-nav">
//
//                    """;
//            this.pairPriorityQueue.add(new PairTest(first, scope.getStartScope()));
//
//            first = """
//                                        </nav>
//                                    </div>
//                                </div>
//                            </div>
//                        </header>
//                    """;
//            this.pairPriorityQueue.add(new PairTest(first, scope.getEndScope() + 0.1));
//        } else {
//            String first = """
//                        <header class="header-area header-sticky">
//                            <div class="container">
//                                <div class="row">
//                                    <div class="col-12">
//                                        <nav class="main-nav">
//
//                    """;
//            this.pairPriorityQueue.add(new PairTest(first, scope.getStartScope()));
//
//            first = """
//                                        </nav>
//                                    </div>
//                                </div>
//                            </div>
//                        </header>
//                    """;
//            this.pairPriorityQueue.add(new PairTest(first, scope.getEndScope()));
//        }
//    }


//    public void addFooter(Footer footer) {
//        Scope scope = footer.getScope();
//        String first;
//        if (scope.getEndScope() == scope.getStartScope()) {
//            first = """
//                    <footer>
//                        <div class="container">
//                            <div class="row">
//
//                    """;
//            this.pairPriorityQueue.add(new PairTest(first, scope.getStartScope()));
//
//            first = """
//                            </div>
//                        </div>
//                    </footer>""";
//
//            this.pairPriorityQueue.add(new PairTest(first, scope.getEndScope() + 0.1));
//        } else {
//            first = """
//                    <footer>
//                        <div class="container">
//                            <div class="row">
//
//                    """;
//            this.pairPriorityQueue.add(new PairTest(first, scope.getStartScope()));
//
//            first = """
//                            </div>
//                        </div>
//                    </footer>""";
//
//            this.pairPriorityQueue.add(new PairTest(first, scope.getEndScope()));
//        }
//    }

    public void appendToHtmlFile_Body(Body body) {
        Scope scope = body.getScope();
        String first;
        first = """
                <body class="login-body">\040\040\040\040\040\040\040\040\040\040\040\040
                <center>
                 """;

        this.pairPriorityQueue.add(new PairTest(first, scope.getStartScope()));

        if (body.getBodyBody() != null) {
            appendBodyBody(body.getBodyBody());
        }
        first = """
                </center>
                </body>
                """;
        this.pairPriorityQueue.add(new PairTest(first, scope.getEndScope()));

    }

    public void appendBodyBody(BodyBody bodyBody) {
        Scope scope = bodyBody.getScope();
        String first;
        first = """
                           
                """;

        this.pairPriorityQueue.add(new PairTest(first, scope.getStartScope()));
        if (bodyBody.getForm() != null) {
            appendForm(bodyBody.getForm());
        }
        first = """
                                
                """;
        this.pairPriorityQueue.add(new PairTest(first, scope.getEndScope()));
    }

    public void appendForm(Form form) {
        Scope scope = form.getScope();
        String first;
        first = "<div class=\"login-page\">\n  " +
                "<div class=\"formStyle\">\n" +
                "<form ";

        this.pairPriorityQueue.add(new PairTest(first, scope.getStartScope()));

        if (form.getAttributeForm() != null) {
            appendAttributeForm(form.getAttributeForm());
        }
        first = ">\n";
        this.pairPriorityQueue.add(new PairTest(first, scope.getStartScope() + 0.3));
        if (form.getBodyForm() != null) {
            for (int i = 0; i < form.getBodyForm().size(); i++) {
                double f = i + 1;
                double s = f / 10.0;
                double res = 0.3 + s;
                appendBodyForm(form.getBodyForm().get(i), new Scope(scope.getStartScope(), scope.getEndScope()), res);
            }
        }
        first = """
                           
                </form>
                </div>
                </div>
                """;
        this.pairPriorityQueue.add(new PairTest(first, scope.getEndScope()));
    }

    public void appendAttributeForm(AttributeForm attributeForm) {
        Scope scope = attributeForm.getScope();
        ////            Attribute Form         ////
        Action action = attributeForm.getAction();
        Method method = attributeForm.getMethod();
        AutoComplete autoComplete = attributeForm.getAutoComplete();
        Target target = attributeForm.getTarget();
        /// ---------------------------------- ////
        if (action != null) {
            appendActionToForm(action, scope);
        }
        if (method != null) {
            appendMethodToForm(method, scope);
        }
        if (autoComplete != null) {
            appendAutoCompleteToForm(autoComplete, scope);
        }
        if (target != null) {
            appendTargetToForm(target, scope);
        }
    }

    public void appendBodyForm(BodyForm bodyForm, Scope scope, double bios) {
        if (bodyForm.getTextField() != null)
            appendTextField(bodyForm.getTextField(), scope, bios);
        else if (bodyForm.getPasswordField() != null)
            appendPasswordField(bodyForm.getPasswordField(), scope, bios + 0.3);
        else if (bodyForm.getDateTimeField() != null)
            appendDateTimeField(bodyForm.getDateTimeField(), scope, bios + 0.4);
        else if (bodyForm.getRadioButton() != null)
            appendRadioButton(bodyForm.getRadioButton(), scope, bios + 0.5);
        else if (bodyForm.getButton() != null)
            appendButtonToForm(bodyForm.getButton(), scope, bios + 0.6);

    }

    public void appendButtonToForm(Button button, Scope scope, double bios) {
        String first;
        if (button.getGoTo() != null) {
            first = "\n<br/>\n<button>\n\t\t<a href=\"";
            first += button.getGoTo().getNamePageGoTo() + ".html\">";
            first += button.getNameButton();
            first += "</a>\n";

            first += "\t</button> \n <br/>";

        } else {
            first = "\n<br/> \n <input type=submit value=\"";
            first += button.getOnPressed().getNameFileControllerOrFun() + "\"/> \n <br/> \n";

        }
        this.pairPriorityQueue.add(new PairTest(first, scope.getStartScope() + bios));
    }

    public void appendActionToForm(Action action, Scope scope) {
        String first = "action=\"";
        first += action.getPathToMakeAction();
        first += ".php\" ";
        String fileNameController = action.getPathToMakeAction() + ".php";
        String pathController = "src/result/" + fileNameController;
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(pathController));
            writer.write("<?php\n");
            writer.close();
        } catch (IOException ioException) {
            System.out.println(ioException.getMessage());
        }
        this.pairPriorityQueue.add(new PairTest(first, scope.getStartScope() + 0.2));
    }

    public void appendMethodToForm(Method method, Scope scope) {
        String first = "method=\"";
        first += method.getMethodType();
        first += "\" ";
        this.pairPriorityQueue.add(new PairTest(first, scope.getStartScope() + 0.2));
    }

    public void appendAutoCompleteToForm(AutoComplete autoComplete, Scope scope) {
        String first = "autoComplete=\"";
        first += autoComplete.getState();
        first += "\" ";
        this.pairPriorityQueue.add(new PairTest(first, scope.getStartScope() + 0.2));
    }


    public void appendTargetToForm(Target target, Scope scope) {
        String first = "target=\"";
        first += target.getTargetEvent();
        first += "\" ";
        this.pairPriorityQueue.add(new PairTest(first, scope.getStartScope() + 0.2));
    }

    public void appendTextField(TextField textField, Scope scope, double bois) {

        String first = "\t<label for=\"";
        first += textField.getTextFieldName();
        first += "\">\n\t\t";

        first += textField.getHint();
        first += "\t\n<br/>\n\t\t<input name=\"";
        first += textField.getTextFieldName();
        first += "\" type = \"text\"";
        if (textField.getValue() != null && !Objects.equals(textField.getValue(), "")) {
            first += " value=\"";
            first += textField.getValue();
            first += "\"";
        }
        first += "/>";
        first += "\n\t</label>\n";
        first += "\t<br>\n";
        this.pairPriorityQueue.add(new PairTest(first, scope.getStartScope() + bois));
    }

    public void appendRadioButton(RadioButton radioButton, Scope scope, double bios) {
        String first = "";
        first += "\t<input type=\"radio\" name=\"" + radioButton.getNameRadioButton() + "\">\n";
        first += "\t<label for=\"";
        first += radioButton.getNameRadioButton();
        first += "\">\n";
        first += "\t\t<br/>\n\t\t";
        first += radioButton.getNameRadioButton();
        first += "\n\t</label>\n";

        first += "\t<br>\n";

        this.pairPriorityQueue.add(new PairTest(first, scope.getStartScope() + bios));
    }

    public void appendDateTimeField(DateTimeField dateTimeField, Scope scope, double bios) {
        String first = "\t<label";
        first += ">\n\t";
        first += dateTimeField.getNameDateTimeField();
        first += "\n<br/>\n\t";
        first += "\t<input type=\"datetime-local\" name=\"" + dateTimeField.getNameDateTimeField() + "\" />";
        first += "</label>\n";

        first += "<br>\n";
        this.pairPriorityQueue.add(new PairTest(first, scope.getStartScope() + bios));
    }

    public void appendPasswordField(PasswordField passwordField, Scope scope, double bios) {

        String first = "\t<label";
        first += ">\n\t<br/>\n\t\n";
        first += "Password";

        first += "<br/>\n\t<input type=\"password\" " + "name=\"" + passwordField.getNameField() + "\" />";
        first += "\t</label>\n";

        first += "<br>\n";
        this.pairPriorityQueue.add(new PairTest(first, scope.getStartScope() + bios));
    }

    public void generate(SymbolTable symbolTable) {

        if (StoreError.counter == 0)
            generatedAllPagesInDoc(symbolTable);
    }
}
