package ast.Node.Page;

import ast.Node.ParentNode;
import ast.Node.Scope;

public class Page extends ParentNode {
    Title title;

    Icon icon;
    AttributeInPage attributeInPage;

    String namePage;

    public Page() {
        namePage = "";
        title = new Title();
        icon = new Icon();
        attributeInPage = new AttributeInPage();
        super.setScope(new Scope());
    }

    public Page(Title title, AttributeInPage attributeInPage, Scope scope, Icon icon) {
        this.title = title;
        this.attributeInPage = attributeInPage;
        super.setScope(scope);
        this.icon = icon;
    }

    public Page(Title title, AttributeInPage attributeInPage, Scope scope, String namePage) {
        this.title = title;
        this.attributeInPage = attributeInPage;
        super.setScope(scope);
        this.namePage = namePage;
    }

    public Page(Scope scope) {
        super.setScope(scope);
    }

    public Title getTitle() {
        return title;
    }

    public void setTitle(Title title) {
        this.title = title;
    }

    public AttributeInPage getAttributeInPage() {
        return attributeInPage;
    }

    public void setAttributeInPage(AttributeInPage attributeInPage) {
        this.attributeInPage = attributeInPage;
    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }

    public String getNamePage() {
        return namePage;
    }

    public void setNamePage(String namePage) {
        this.namePage = namePage;
    }

    public Icon getIcon() {
        return icon;
    }

    public void setIcon(Icon icon) {
        this.icon = icon;
    }

}
