package ast.Node.Body.Form.Attribute;

import ast.Node.ParentNode;
import ast.Node.Scope;

public class Method extends ParentNode {
    String methodType;


    public Method() {
    }

    public Method(String methodType, Scope scope) {
        this.methodType = methodType;
        super.setScope(scope);
    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }

    public String getMethodType() {
        return methodType;
    }

    public void setMethodType(String methodType) {
        this.methodType = methodType;
    }

}
