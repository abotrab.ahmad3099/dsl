package ast.Node.controller;

import ast.Node.ParentNode;
import ast.Node.Scope;

public class WriteOnFileData extends ParentNode {

    Object data;

    public WriteOnFileData() {
    }

    public WriteOnFileData(Scope scope, Object data) {
        super.setScope(scope);
        this.data = data;
    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
