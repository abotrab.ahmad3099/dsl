package ast.Node.controller;

import ast.Node.ParentNode;
import ast.Node.Scope;

public class And extends ParentNode {
    String textAnd;

    public And() {
    }

    public And(String textAnd, Scope scope) {
        this.textAnd = textAnd;
        super.setScope(scope);
    }

    public String getTextAnd() {
        return textAnd;
    }

    public void setTextAnd(String textAnd) {
        this.textAnd = textAnd;
    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }

}
