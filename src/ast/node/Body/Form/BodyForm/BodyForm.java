package ast.Node.Body.Form.BodyForm;

import ast.Node.ParentNode;
import ast.Node.Scope;

public class BodyForm extends ParentNode {
    String nameBodyForm;
    TextField textField;
    Button button;

    RadioButton RadioButton;

    PasswordField PasswordField;

    DateTimeField dateTimeField;


    public BodyForm() {
    }

    public BodyForm(String nameBodyForm, TextField textField, Button button,
                    RadioButton radioButton, PasswordField passwordField, DateTimeField dateTimeField,
                    Scope scope) {
        this.nameBodyForm = nameBodyForm;
        this.textField = textField;
        this.button = button;
        this.RadioButton = radioButton;
        this.PasswordField = passwordField;
        this.dateTimeField = dateTimeField;
        super.setScope(scope);
    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }

    public String getNameBodyForm() {
        return nameBodyForm;
    }

    public void setNameBodyForm(String nameBodyForm) {
        this.nameBodyForm = nameBodyForm;
    }

    public TextField getTextField() {
        return textField;
    }

    public void setTextField(TextField textField) {
        this.textField = textField;
    }

    public Button getButton() {
        return button;
    }

    public void setButton(Button button) {
        this.button = button;
    }

    public RadioButton getRadioButton() {
        return RadioButton;
    }

    public void setRadioButton(RadioButton radioButton) {
        RadioButton = radioButton;
    }

    public PasswordField getPasswordField() {
        return PasswordField;
    }

    public void setPasswordField(PasswordField passwordField) {
        PasswordField = passwordField;
    }

    public DateTimeField getDateTimeField() {
        return dateTimeField;
    }

    public void setDateTimeField(DateTimeField dateTimeField) {
        this.dateTimeField = dateTimeField;
    }
}
