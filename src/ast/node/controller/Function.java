package ast.Node.controller;

import ast.Node.ParentNode;
import ast.Node.Scope;

import java.util.ArrayList;

public class Function extends ParentNode {
    String nameFunction;
    ArrayList<Object> parameter;

    ArrayList<BlockStatement> blockStatement;

    public Function() {
        nameFunction = "";
        parameter = new ArrayList<>();
        super.setScope(new Scope(0, 0));
        blockStatement = new ArrayList<>();
    }

    public Function(String nameFunction, ArrayList<Object> parameter, Scope scope, ArrayList<BlockStatement> blockStatement) {
        this.nameFunction = nameFunction;
        this.parameter = parameter;
        super.setScope(scope);
        this.blockStatement = blockStatement;
    }

    public String getNameFunction() {
        return nameFunction;
    }

    public void setNameFunction(String nameFunction) {
        this.nameFunction = nameFunction;
    }

    public ArrayList<Object> getParameter() {
        return parameter;
    }

    public void setParameter(ArrayList<Object> parameter) {
        this.parameter = parameter;
    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }

    public ArrayList<BlockStatement> getBlockStatement() {
        return blockStatement;
    }

    public void setBlockStatement(ArrayList<BlockStatement> blockStatement) {
        this.blockStatement = blockStatement;
    }

}
