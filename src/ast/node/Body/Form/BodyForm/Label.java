package ast.Node.Body.Form.BodyForm;

import ast.Node.ParentNode;
import ast.Node.Scope;

public class Label extends ParentNode {
    String valueTextInLable;


    public Label() {
    }

    public Label(String valueTextInLable, Scope scope) {
        this.valueTextInLable = valueTextInLable;
        super.setScope(scope);
    }

    public Label(Scope scope) {
        super.setScope(scope);
    }

    public String getValueTextInLable() {
        return valueTextInLable;
    }

    public void setValueTextInLable(String valueTextInLable) {
        this.valueTextInLable = valueTextInLable;
    }

    public Scope getScope() {
        return super.getScope();
    }

    public void setScope(Scope scope) {
        super.setScope(scope);
    }
}
